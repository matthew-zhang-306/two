#include <iostream>
#include <SDL.h>
#include "core/two.h"
#include "game/scenes/physicstestscene.h"

using namespace std;
using namespace two;

int main(int argc, char *argv[])
{
    Core core;

    core.init();
    core.loadScene<PhysicsTestScene>();
    core.run();
    core.destroy();

    return 0;
}