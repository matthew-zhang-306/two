#include "cameracontroller.h"

using namespace std;
using namespace two;

CameraController::CameraController(Player* _player) {
  player = _player;
  
  defaultPosition = Vec2s::Zero;
  defaultWidth = 0.0f;
  blend = 0.0f;
  isOnPlayer = false;
}

CameraController::~CameraController() {

}


void CameraController::onStart() {
  defaultPosition = getScene()->getCamera()->getPosition();
  defaultWidth = getScene()->getCamera()->getWidth();
}

void CameraController::update(float delta) {
  Input* input = getScene()->getCore()->getInput();
  Camera* camera = getScene()->getCamera();

  if (input->getKeyPressed(KeyCode::LShift)) {
    isOnPlayer = !isOnPlayer;
  }

  blend = Mathf::lerp(blend, isOnPlayer ? 1.0f : 0.0f, 0.05f);
  camera->setPosition(Vec2::lerp(defaultPosition, player->getTransform()->getPosition(), blend));
  camera->setWidth(Mathf::lerp(defaultWidth, defaultWidth / 2.0f, blend));
}
