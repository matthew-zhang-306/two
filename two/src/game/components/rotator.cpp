#include "rotator.h"
#include <iostream>

Rotator::Rotator(float _rotateSpeed) {
  rotateSpeed = _rotateSpeed;
}

Rotator::~Rotator() {

}


void Rotator::update(float delta) {
  getTransform()->rotateBy(rotateSpeed * delta);
  for (Transform* child : getTransform()->getChildren()) {
    child->setRotation(0.0f);
  }
}