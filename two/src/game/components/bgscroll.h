#pragma once
#include "../../core/two.h"

using namespace two;
using namespace std;

class BGScroll : public Component {
  private:
    float scrollSpeed;

  public:
    BGScroll(float _scrollSpeed = 0.0f);
    ~BGScroll();

    void update(float delta) override;
};