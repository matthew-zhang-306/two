#pragma once
#include "../../core/two.h"

using namespace two;
using namespace std;

class PhysicsBall : public Component {
  private:
    float speedMultiplier = 1.0f;
    float hSpeed = 0.0f;
    float jumpSpeed = 0.0f;
    float gravity = 0.0f;
    Vec2 velocity = Vec2s::Zero;

    ActorBody* actor;

  public:
    PhysicsBall(float _speedMultiplier = 1.0f);
    ~PhysicsBall();

    void onStart() override;
    void update(float delta) override;
};