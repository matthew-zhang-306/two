#pragma once
#include "../../core/two.h"

using namespace two;
using namespace std;

class Player : public Component {
  private:
    float groundHeight = 0.0f;
    float maxMoveSpeed = 0.0f;
    float groundAccel = 0.0f;
    float airAccel = 0.0f;
    float jumpSpeed = 0.0f;
    float gravity = 0.0f;
    float maxFallSpeed = 0.0f;
    Vec2 velocity = Vec2s::Zero;

    bool isGrounded = false;
    float groundBuffer = 0.0f;
    float jumpBuffer = 0.0f;

    SpriteRenderer* spriteRenderer;
    ActorBody* actor;

  public:
    Player();
    ~Player();

    void onStart() override;
    void update(float delta) override;
};