#pragma once
#include "../../core/two.h"

using namespace two;
using namespace std;

class Rotator : public Component {
  private:
    float rotateSpeed;

  public:
    Rotator(float _rotateSpeed = 0.0f);
    ~Rotator();

    SET_UPDATE_ORDER(-1)

    void update(float delta) override;
};