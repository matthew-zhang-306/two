#pragma once
#include "../../core/two.h"
#include "player.h"

using namespace two;
using namespace std;

class CameraController : public Component {
  private:
    Vec2 defaultPosition;
    float defaultWidth;
    Player* player;
    float blend;
    bool isOnPlayer;

  public:
    CameraController(Player* _player = NULL);
    ~CameraController();

    SET_UPDATE_ORDER(1000);

    void onStart() override;
    void update(float delta) override;
};