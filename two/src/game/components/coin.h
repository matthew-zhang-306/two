#pragma once
#include "../../core/two.h"

using namespace two;
using namespace std;

class Coin : public Component {
  private:
    BoxCollider* boxCollider;
    BoxCollider* playerCollider; // TODO: make some kind of safe pointer??

  public:
    Coin(BoxCollider* _playerCollider = NULL);
    ~Coin();

    void onStart() override;
    void update(float delta) override;
};