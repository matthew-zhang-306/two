#include "selectmenu.h"
#include "../scenes/testscene.h"

using namespace two;
using namespace std;

void SelectMenu::update(float delta) {
  Input* input = getScene()->getCore()->getInput();
  if (input->getKeyPressed(KeyCode::D1)) {
    getScene()->getCore()->loadScene<TestScene>(0);
  }
  else if (input->getKeyPressed(KeyCode::D2)) {
    getScene()->getCore()->loadScene<TestScene>(1);
  }
  else if (input->getKeyPressed(KeyCode::D3)) {
    getScene()->getCore()->loadScene<TestScene>(2);
  }
}