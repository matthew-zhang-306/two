#include "coin.h"
#include <iostream>

Coin::Coin(BoxCollider* _playerCollider) {
  boxCollider = NULL;
  playerCollider = _playerCollider;
}

Coin::~Coin() {
  
}


void Coin::onStart() {
  boxCollider = getEntity().getComponent<BoxCollider>();
}

void Coin::update(float delta) {
  if (playerCollider != NULL &&
      playerCollider->getBounds().intersects(boxCollider->getBounds()))
  {
    // collect coin
    Debug::log("Coin::update", "coin " + Debug::u(getEntity().getId()) + " being collected");
    getEntity().destroy();
  }
}