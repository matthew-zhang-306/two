#include "bgscroll.h"
#include <iostream>

BGScroll::BGScroll(float _rotateSpeed) {
  scrollSpeed = _rotateSpeed;
}

BGScroll::~BGScroll() {

}


void BGScroll::update(float delta) {
  getTransform()->movePosition(Vec2(0.0f, scrollSpeed * delta));

  Vec2 windowSize = getScene()->getCore()->getRenderer()->getWindowSize();
  if (getTransform()->getPosition().y < 0.0f) {
    getTransform()->movePosition(Vec2(0.0f, windowSize.y));
  }
  else if (getTransform()->getPosition().y > windowSize.y) {
    getTransform()->movePosition(-Vec2(0.0f, windowSize.y));
  }
}