#include "player.h"
#include "../scenes/selectscene.h"
#include <iostream>

Player::Player() {
  actor = NULL;
}

Player::~Player() {

}


void Player::onStart() {
  groundHeight = getTransform()->getPosition().y;
  maxMoveSpeed = 80.0f;
  groundAccel = 950.0f;
  airAccel = 235.0f;
  jumpSpeed = 150.0f;
  gravity = 410.0f;
  maxFallSpeed = 1000.0f;

  spriteRenderer = getEntity().getComponent<SpriteRenderer>();
  actor = getEntity().getComponent<ActorBody>();
}

void Player::update(float delta) {
  // input gathering
  Input* input = getScene()->getCore()->getInput();
  float x = 0;
  x += input->getKeyHeld(KeyCode::D) ? 1.0f : 0.0f;
  x -= input->getKeyHeld(KeyCode::A) ? 1.0f : 0.0f;
  bool j = input->getKeyHeld(KeyCode::J);
  bool oldJ = input->getKeyWasHeld(KeyCode::J);

  jumpBuffer = j && !oldJ ? 0.1f : max(0.0f, jumpBuffer - delta);
  groundBuffer = isGrounded ? 0.1f : max(0.0f, groundBuffer - delta);

  // physics!!!
  Vec2 vel = velocity;
  vel.x = Mathf::moveToward(vel.x, maxMoveSpeed * x, (isGrounded ? groundAccel : airAccel) * delta);
  vel.y = Mathf::moveToward(vel.y, maxFallSpeed, gravity * delta);
  if (jumpBuffer > 0.0f && groundBuffer > 0.0f) {
    vel.y = -jumpSpeed;
    jumpBuffer = 0.0f;
    groundBuffer = 0.0f;
  }
  velocity = vel;

  int slideCount = 1;
  Vec2 moveAmount = velocity * delta;
  actor->move(moveAmount);

  isGrounded = false;
  while (actor->hasPreviousCollision()) {
    if (actor->getPreviousCollisionNormal().y < 0.0f) {
      // hit ground
      isGrounded = true;
      velocity.y = 0.0f;
      Debug::log("Player::update", "player is on ground " + Debug::u(actor->getPreviousCollisionSolid()->getEntity().getId()), DebugLevel::Medium);
    }

    if (slideCount > 0) {
      actor->slide(moveAmount);
      slideCount--;
    }
    else {
      break;
    }
  }

  
  // flip
  if (x != 0.0f && (x < 0.0f) != (spriteRenderer->getFlipX())) {
    spriteRenderer->setFlipX(x < 0.0f);
  }


  // quit
  if (input->getKeyPressed(KeyCode::Esc)) {
    getScene()->getCore()->loadScene<SelectScene>();
  }
}