#include "physicsball.h"

PhysicsBall::PhysicsBall(float _speedMultiplier) {
  speedMultiplier = _speedMultiplier;
  actor = NULL;
}

PhysicsBall::~PhysicsBall() {

}


void PhysicsBall::onStart() {
  hSpeed = 150.0f;
  jumpSpeed = 250.0f;
  gravity = 600.0f;

  velocity = Vec2(hSpeed, 0.0f);

  actor = getEntity().getComponent<ActorBody>();
}

void PhysicsBall::update(float delta) {
  float scaledDelta = delta * speedMultiplier;
  
  velocity.y += gravity * scaledDelta;

  int bounceCount = 1;
  Vec2 moveAmount = velocity * scaledDelta;
  actor->move(moveAmount);

  while (actor->hasPreviousCollision()) {
    if (actor->getPreviousCollisionNormal().y < 0.0f) {
      // hit ground
      velocity.y = -jumpSpeed;
    }
    if (actor->getPreviousCollisionNormal().y > 0.0f) {
      // hit ceiling
      velocity.y = 0.0f;
    }
    else if (actor->getPreviousCollisionNormal().x != 0.0f) {
      // hit wall
      velocity.x = -velocity.x;
    }

    if (bounceCount > 0) {
      actor->move(velocity * scaledDelta * actor->getPreviousCollisionRemainder());
      bounceCount--;
    }
    else {
      break;
    }
  }
}