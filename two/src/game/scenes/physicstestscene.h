#pragma once
#include "../../core/two.h"

using namespace std;
using namespace two;

class PhysicsTestScene : public Scene {
  private:
    const static Vec2 TileSize;

    Entity placeBall(Vec2 tilePos, float size, float speedMultiplier);
    Entity placeGround(Rect2 tileRect);
    Entity placeSlope(Rect2 tileRect, Vec2 orientation);

  public:
    PhysicsTestScene() {}
    ~PhysicsTestScene() {}

    void load(Core* _core) override;
    void unload() override;
};

