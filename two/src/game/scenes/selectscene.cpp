#include "selectscene.h"
#include "../components/player.h"
#include "../components/rotator.h"
#include "../components/selectmenu.h"
#include "../components/bgscroll.h"

using namespace std;
using namespace two;

void SelectScene::load(Core* _core) {
  Scene::load(_core);

  AssetManager* assets = core->getAssetManager();
  Renderer* renderer = core->getRenderer();

  // load assets
  if (!assets->hasTexture("bgdim")) {
    assets->loadTexture(renderer, "bgdim", "./assets/bgdim.png");
  }
  if (!assets->hasTexture("select")) {
    assets->loadTexture(renderer, "select", "./assets/select.png");
  }

  // make level
  Entity a = createEntity();
  a.addComponent<SelectMenu>();

  Entity bg = createEntity();
  Rect2 bgRect = Rect2(0.0f, 0.0f, 320.0f, 384.0f);
  bg.getTransform()->setPosition(bgRect.center());
  bg.addComponent<SpriteRenderer>("bgdim", bgRect.size(), 1.0f);
  bg.addComponent<BGScroll>(-5.0f);

  Entity c = createEntity();
  c.getTransform()->setPosition(renderer->getWindowSize() / 2.0f);
  c.addComponent<SpriteRenderer>("select", renderer->getWindowSize(), 1.0f);
}

void SelectScene::unload() {
  Scene::unload();
}