#include "physicstestscene.h"
#include "../components/gamecomponents.h"

using namespace std;
using namespace two;

const Vec2 PhysicsTestScene::TileSize = Vec2(16.0f, 16.0f);

void PhysicsTestScene::load(Core* _core) {
  Scene::load(_core);

  AssetManager* assets = core->getAssetManager();
  Renderer* renderer = core->getRenderer();
  assets->loadTexture(renderer, "ball", "./assets/ball.png");
  assets->loadTexture(renderer, "square", "./assets/square.png");

  placeGround(Rect2(0.0f, 0.0f, 1.0f, 12.0f));
  placeGround(Rect2(19.0f, 0.0f, 1.0f, 12.0f));
  placeGround(Rect2(0.0f, 11.0f, 20.0f, 1.0f));
  placeGround(Rect2(5.3f, 8.9f, 6.8f, 3.1f));
  placeGround(Rect2(14.8f, 7.2f, 5.0f, 0.7f));
  placeGround(Rect2(0.0f, 3.3f, 6.4f, 1.2f));
  placeBall(Vec2(4.0f, 4.0f), 1.5f, 1.0f);
  placeBall(Vec2(8.2f, 4.0f), 0.42f, 2.7f);
  placeBall(Vec2(9.1f, 4.0f), 3.1f, 0.23f);

  placeSlope(Rect2(5.1f, 0.9f, 5.0f, 2.0f), Vec2(-1.0f, -1.0f));
  placeSlope(Rect2(6.1f, 3.9f, 3.0f, 5.0f), Vec2(1.0f, -1.0f));
}

void PhysicsTestScene::unload() {
  AssetManager* assets = core->getAssetManager();
  assets->unloadTexture("ball");
  assets->unloadTexture("square");
  Scene::unload();
}


Entity PhysicsTestScene::placeBall(Vec2 tilePos, float size, float speedMultiplier) {
  Entity a = createEntity();
  a.getTransform()->setPosition((tilePos + Vec2s::Half) * TileSize);
  a.getTransform()->setScale(Vec2s::One * size);
  auto* sprite = a.addComponent<SpriteRenderer>("ball", Vec2(64.0f, 64.0f), 0.25f);
  sprite->setSortOrder(10);
  a.addComponent<BoxCollider>(Vec2(16.0f, 16.0f));
  a.addComponent<ActorBody>();
  a.addComponent<PhysicsBall>(speedMultiplier);
  return a;
}

Entity PhysicsTestScene::placeGround(Rect2 tileRect) {
  Rect2 worldRect = Rect2::fromMinMax(tileRect.min() * TileSize, tileRect.max() * TileSize);

  Entity b = createEntity();
  b.getTransform()->setPosition(worldRect.center());
  b.getTransform()->setScale(worldRect.size() / 16.0f);
  auto* sprite = b.addComponent<SpriteRenderer>("square", Vec2(16.0f, 16.0f), 1.0f);
  sprite->setSortOrder(0);
  b.addComponent<BoxCollider>(Vec2(16.0f, 16.0f));
  b.addComponent<SolidBody>();
  return b;
}

Entity PhysicsTestScene::placeSlope(Rect2 tileRect, Vec2 orientation) {
  Rect2 worldRect = Rect2::fromMinMax(tileRect.min() * TileSize, tileRect.max() * TileSize);

  Entity c = createEntity();
  c.getTransform()->setPosition(worldRect.center());
  c.getTransform()->setScale(worldRect.size() / 16.0f);
  c.addComponent<SlopeCollider>(Vec2(16.0f, 16.0f), orientation);
  // c.addComponent<SolidBody>();
  return c;
}
