#pragma once
#include "../../core/two.h"

using namespace std;
using namespace two;

class SelectScene : public Scene {
  public:
    SelectScene() {}
    ~SelectScene() {}

    void load(Core* _core) override;
    void unload() override;
};
