#include "testscene.h"
#include "../components/gamecomponents.h"

using namespace std;
using namespace two;

const Vec2 TestScene::TileSize = Vec2(16.0f, 16.0f);

void TestScene::load(Core* _core) {
  Scene::load(_core);

  AssetManager* assets = core->getAssetManager();
  Renderer* renderer = core->getRenderer();

  // load assets
  assets->loadTexture(renderer, "bg", "./assets/bg.png");
  if (levelNumber == 0) {
    assets->loadTexture(renderer, "player", "./assets/playerBlue.png");
    assets->loadTexture(renderer, "tile", "./assets/tileBlue.png");
    assets->loadTexture(renderer, "fill", "./assets/fillBlue.png");
  }
  else if (levelNumber == 1) {
    assets->loadTexture(renderer, "player", "./assets/playerGreen.png");
    assets->loadTexture(renderer, "tile", "./assets/tileGreen.png");
    assets->loadTexture(renderer, "fill", "./assets/fillGreen.png");
  }
  else if (levelNumber == 2) {
    assets->loadTexture(renderer, "player", "./assets/playerRed.png");
    assets->loadTexture(renderer, "tile", "./assets/tileRed.png");
    assets->loadTexture(renderer, "fill", "./assets/fillRed.png");
  }
  assets->loadTexture(renderer, "coin", "./assets/coin.png");

  // make level
  SpriteRenderer* playerSprite = NULL;
  BoxCollider* playerCollider = NULL;
  Player* player = NULL;

  if (levelNumber == 0) {
    Entity a = createEntity();
    a.getTransform()->setPosition(Vec2(4.0f, 7.0f) * TileSize);
    playerSprite = a.addComponent<SpriteRenderer>("player", Vec2(40.0f, 40.0f), 8.0f / 40.0f);
    playerSprite->setSortOrder(10);
    playerCollider = a.addComponent<BoxCollider>(Vec2(8.0f, 8.0f));
    a.addComponent<ActorBody>();
    player = a.addComponent<Player>();

    placeFill(Vec2(3.0f, 8.0f));
    placeFill(Vec2(5.0f, 8.0f));
    placeFill(Vec2(9.0f, 7.0f));
    placeFill(Vec2(11.0f, 7.0f));
    placeFill(Vec2(15.0f, 8.0f));
    placeFill(Vec2(17.0f, 8.0f));

    placeTile(Vec2(3.0f, 8.0f));
    placeTile(Vec2(5.0f, 8.0f));
    placeTile(Vec2(9.0f, 7.0f));
    placeTile(Vec2(11.0f, 7.0f));
    placeTile(Vec2(15.0f, 8.0f));
    placeTile(Vec2(17.0f, 8.0f));

    placeCoinRing(Vec2(6.5f, 5.5f), playerCollider);
    placeCoinRing(Vec2(13.5f, 5.5f), playerCollider);
  }
  else if (levelNumber == 1) {
    Entity a = createEntity();
    a.getTransform()->setPosition(Vec2(3.0f, 8.0f) * TileSize);
    playerSprite = a.addComponent<SpriteRenderer>("player", Vec2(38.0f, 50.0f), 8.0f / 38.0f);
    playerSprite->setSortOrder(10);
    playerCollider = a.addComponent<BoxCollider>(Vec2(8.0f, 8.0f * 50.0f / 38.0f));
    a.addComponent<ActorBody>();
    player = a.addComponent<Player>();

    placeFill(Vec2(3.0f, 9.0f));
    placeFill(Vec2(5.0f, 8.0f));
    placeFill(Vec2(7.0f, 7.0f));
    placeFill(Vec2(10.0f, 5.5f));
    placeFill(Vec2(13.0f, 4.0f));
    placeFill(Vec2(15.0f, 3.0f));
    placeFill(Vec2(17.0f, 2.0f));

    placeTile(Vec2(3.0f, 9.0f));
    placeTile(Vec2(5.0f, 8.0f));
    placeTile(Vec2(7.0f, 7.0f));
    placeTile(Vec2(10.0f, 5.5f));
    placeTile(Vec2(13.0f, 4.0f));
    placeTile(Vec2(15.0f, 3.0f));
    placeTile(Vec2(17.0f, 2.0f));

    placeCoinRing(Vec2(5.0f, 4.5f), playerCollider);
    placeCoinRing(Vec2(10.0f, 2.0f), playerCollider);
  }
  else if (levelNumber == 2) {
    Entity a = createEntity();
    a.getTransform()->setPosition(Vec2(3.0f, 2.0f) * TileSize);
    playerSprite = a.addComponent<SpriteRenderer>("player", Vec2(47.0f, 50.0f), 8.0f / 47.0f);
    playerSprite->setSortOrder(10);
    playerCollider = a.addComponent<BoxCollider>(Vec2(8.0f, 8.0f));
    a.addComponent<ActorBody>();
    player = a.addComponent<Player>();

    placeFill(Vec2(3.0f, 3.0f));
    placeFill(Vec2(10.0f, 9.0f));
    placeFill(Vec2(14.0f, 4.5f));
    placeFill(Vec2(17.0f, 3.0f));

    placeTile(Vec2(3.0f, 3.0f));
    placeTile(Vec2(10.0f, 9.0f));
    placeTile(Vec2(14.0f, 7.5f));
    placeTile(Vec2(17.0f, 6.0f));
    placeTile(Vec2(14.0f, 4.5f));
    placeTile(Vec2(17.0f, 3.0f));

    placeCoinRing(Vec2(10.0f, 4.0f), playerCollider);
  }

  Entity cam = createEntity();
  cam.addComponent<CameraController>(player);

  Entity bg = createEntity();
  Rect2 bgRect = Rect2(0.0f, 0.0f, 320.0f, 384.0f);
  bg.getTransform()->setPosition(bgRect.center());
  auto* sprite = bg.addComponent<SpriteRenderer>("bg", bgRect.size(), 1.0f);
  sprite->setSortOrder(-10);
  bg.addComponent<BGScroll>(-5.0f);
}

void TestScene::unload() {
  core->getAssetManager()->unloadTexture("bg");
  core->getAssetManager()->unloadTexture("player");
  core->getAssetManager()->unloadTexture("tile");
  core->getAssetManager()->unloadTexture("fill");
  core->getAssetManager()->unloadTexture("coin");
  Scene::unload();
}


Entity TestScene::placeFill(Vec2 tilePos) {
  Rect2 fillRect = Rect2::fromMinMax(
    Vec2((tilePos.x - 1) * TileSize.x, (tilePos.y + 0.5f) * TileSize.y),
    Vec2((tilePos.x + 1) * TileSize.x, getCore()->getRenderer()->getWindowSize().y)
  );

  Entity fill = createEntity();
  fill.getTransform()->setPosition(fillRect.center());
  fill.getTransform()->setScale(Vec2(fillRect.w / 64.0f, fillRect.h / 64.0f));
  fill.addComponent<SpriteRenderer>("fill", Vec2(64.0f, 64.0f), 1.0f);
  return fill;
}

Entity TestScene::placeTile(Vec2 tilePos) {
  Entity tile = createEntity();
  tile.getTransform()->setPosition(tilePos * TileSize + Vec2(0.0f, 50.0f * 0.125f));
  tile.addComponent<SpriteRenderer>("tile", Vec2(128.0f, 50.0f), 0.25f);
  tile.addComponent<BoxCollider>(Vec2(32.0f, 50.0f * 0.25f));
  tile.addComponent<SolidBody>();
  return tile;
}

Entity TestScene::placeCoinRing(Vec2 tilePos, BoxCollider* playerCollider) {
  Entity ring = createEntity();
  ring.getTransform()->setPosition(tilePos * TileSize);
  ring.addComponent<Rotator>(60.0f);
  for (int i = 0; i < 8; i++) {
    Entity e = createEntity();
    e.getTransform()->setParent(ring.getTransform());
    e.getTransform()->setLocalPosition(Vec2(15.0f, 0.0f).rotated((float)(i * 45)));
    e.addComponent<SpriteRenderer>("coin", Vec2(22.0f, 22.0f), 5.0f / 22.0f);
    e.addComponent<BoxCollider>(Vec2(6.0f, 6.0f));
    e.addComponent<Coin>(playerCollider);
  }
  return ring;
}