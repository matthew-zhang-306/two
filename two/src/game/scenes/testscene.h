#pragma once
#include "../../core/two.h"

using namespace std;
using namespace two;

class TestScene : public Scene {
  private:
    int levelNumber;
    const static Vec2 TileSize;
  
    Entity placeFill(Vec2 tilePos);
    Entity placeTile(Vec2 tilePos);
    Entity placeCoinRing(Vec2 tilePos, BoxCollider* playerCollider);

  public:
    TestScene(int _levelNumber = 0) {
      levelNumber = _levelNumber;
    }
    ~TestScene() {}

    void load(Core* _core) override;
    void unload() override;
};

