#include "texturedef.h"
#include <SDL_image.h>
#include "../debug/debug.h"

using namespace std;
using namespace two;

TextureDef::TextureDef(string _filePath, Rect2 _textureRect) {
  filePath = _filePath;
  textureRect = _textureRect;
  sdlTexture = NULL;
}

TextureDef::~TextureDef() {
  
}


void TextureDef::load(Renderer* renderer) {
  SDL_Surface* surface = IMG_Load(filePath.c_str());
  sdlTexture = SDL_CreateTextureFromSurface(renderer->getSDLRenderer(), surface);
  SDL_FreeSurface(surface);
}

void TextureDef::unload() {
  SDL_DestroyTexture(sdlTexture);
  sdlTexture = NULL;
}

bool TextureDef::isLoaded() {
  return sdlTexture != NULL;
}

SDL_Texture* TextureDef::getSDLTexture() {
  return sdlTexture;
}


void TextureDef::addSrcRect(Rect2 rect) {
  if (!textureRect.contains(rect)) {
    Debug::warn("TextureDef::addSrcRect", "source rect " + rect + " is not contained within the texture size " + textureRect + ". the source rect will be clamped inside");
    rect = rect.overlap(textureRect);
  }

  srcRects.push_back(rect);
}

int TextureDef::getNumSrcRects() {
  return srcRects.size();
}

Rect2 TextureDef::getSrcRect(int index) {
  if (index < 0 || srcRects.size() >= (Uint32)index) {
    Debug::error("TextureDef::getSrcRect", "source rect index " + Debug::i(index) + " is out of bounds! (num rects = " + Debug::u(srcRects.size()) + ")");
    return Rect2s::Empty;
  }

  return srcRects[index];
}

Rect2 TextureDef::getTextureRect() {
  return textureRect;
}