#pragma once
#include "../rendering/rendering.h"
#include <SDL.h>
#include <string>
#include <unordered_map>

using namespace std;

namespace two {
  class AssetManager {
    private:
      unordered_map<string, SDL_Texture*> textures;
    
    public:
      AssetManager();
      ~AssetManager();

      SDL_Texture* loadTexture(Renderer* renderer, string name, string filePath);
      void unloadTexture(string name);
      bool hasTexture(string name);
      SDL_Texture* getTexture(string name);
      
      void unloadEverything();
  };
}
