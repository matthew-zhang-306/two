#include "assetmanager.h"
#include "../debug/debug.h"
#include <SDL_image.h>

using namespace std;
using namespace two;

AssetManager::AssetManager() {

}

AssetManager::~AssetManager() {
  unloadEverything();
}

SDL_Texture* AssetManager::loadTexture(Renderer* renderer, string name, string filePath) {
  if (hasTexture(name)) {
    Debug::warn("AssetManager::loadTexture", "texture " + name + " already exists!");
    return textures[name];
  }

  SDL_Surface* surface = IMG_Load(filePath.c_str());
  if (surface == NULL) {
    Debug::error("AssetManager::loadTexture", "failed to load image file at " + filePath);
    return NULL;
  }
  SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer->getSDLRenderer(), surface);
  if (texture == NULL) {
    Debug::error("AssetManager::loadTexture", "failed at CreateTextureFromSurface for image file " + filePath);
    SDL_FreeSurface(surface);
    return NULL;
  }
  
  textures.emplace(name, texture);
  SDL_FreeSurface(surface);
  return texture;
}

void AssetManager::unloadTexture(string name) {
  if (!hasTexture(name)) {
    Debug::warn("AssetManager::unloadTexture", "texture " + name + " doesn't exist!");
    return;
  }

  SDL_DestroyTexture(textures[name]);
  textures.erase(name);
}

bool AssetManager::hasTexture(string name) {
  return textures.find(name) != textures.end();
}

SDL_Texture* AssetManager::getTexture(string name) {
  if (!hasTexture(name)) {
    Debug::warn("AssetManager::getTexture", "texture " + name + " doesn't exist!");
    return NULL;
  }

  return textures[name];
}


void AssetManager::unloadEverything() {
  for (pair<string, SDL_Texture*> texture : textures) {
    SDL_DestroyTexture(texture.second);
  }
  textures.clear();
}