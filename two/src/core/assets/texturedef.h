#pragma once
#include <SDL.h>
#include <string>
#include <vector>
#include "../math/math.h"
#include "../rendering/rendering.h"

using namespace std;

namespace two {
  class TextureDef {
    private:
      string filePath;
      vector<Rect2> srcRects;

      Rect2 textureRect;
      SDL_Texture* sdlTexture;
  
    public:
      TextureDef(string _filePath, Rect2 _textureRect);
      ~TextureDef();
   
      void load(Renderer* renderer);
      void unload();
      bool isLoaded();
      SDL_Texture* getSDLTexture();
    
      void addSrcRect(Rect2 rect);
      int getNumSrcRects();
      Rect2 getSrcRect(int index);
      Rect2 getTextureRect();
  };
}