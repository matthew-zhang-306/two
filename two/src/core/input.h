#pragma once
#include <SDL.h>
#include <bitset>
#include <unordered_map>

using namespace std;

namespace two {

  class Core;

  enum KeyCode {
    // arrow keys
    Left,
    Right,
    Down,
    Up,
    // letters
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    // numbers
    D0, D1, D2, D3, D4, D5, D6, D7, D8, D9,
    // buttons
    Space, LShift, LCtrl, LAlt, RShift, RCtrl, RAlt,
    // navigation
    Enter, Backspace, Esc,
    // debug
    Slash, Backslash,

    // this indicates how many keycodes there are
    Unknown
  };

  typedef
    bitset<static_cast<size_t>(KeyCode::Unknown) + 1>
    inputbitset;

  class Input {
    private:
      unordered_map<SDL_Keycode, KeyCode> sdlkToKeyCode;

      inputbitset currentInput;
      inputbitset previousInput;

    public:
      Input();
      ~Input();

      void processInput(Core* core);

      bool getKeyHeld(KeyCode keyCode);
      bool getKeyWasHeld(KeyCode keyCode);
      bool getKeyPressed(KeyCode keyCode);
      bool getKeyReleased(KeyCode keyCode);
  };
}