#pragma once
#include <memory>
#include <SDL.h>
#include "assets/assetmanager.h"
#include "debug/debug.h"
#include "input.h"
#include "rendering/rendering.h"
#include "sec/sec.h"
#include <iostream>

using namespace std;

const float MINIMUM_FPS = 15.0f;
const float TARGET_FPS = 60.0f;
const int WINDOW_WIDTH = 40*8;
const int WINDOW_HEIGHT = 24*8;

namespace two {
  class Core {
    private:
      bool isRunning;
      uint32_t oldTicks;

      AssetManager* assetManager;
      Input* input;
      Renderer* renderer;
      Scene* currentScene;
      Scene* nextScene;

    protected:
      void setup();
      void doECLifeCycle();
      float waitForFrame();
      void update(float dt);
      void render();
      void switchScenes();
      void exit();

    public:
      Core();
      virtual ~Core();

      AssetManager* getAssetManager();
      Input* getInput();
      Renderer* getRenderer();
      Scene* getCurrentScene();

      template <typename TScene, typename ...TArgs>
        TScene* loadScene(TArgs&& ...args);

      void init();
      void run();
      void destroy();

      void toggleDebug();
      void suspend();
      void quitGame();
  };


  template <typename TScene, typename ...TArgs>
  TScene* Core::loadScene(TArgs&& ...args) {
    if (nextScene != NULL) {
      Debug::warn("Core::loadScene", "loading two scenes in one frame. will ignore this scene load");
      return NULL;
    }

    TScene* newTScene = new TScene(forward<TArgs>(args)...);
    nextScene = static_cast<Scene*>(newTScene);

    return newTScene;
  }
}