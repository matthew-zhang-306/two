#pragma once
#include <vector>
#include <SDL.h>
#include "vec2.h"

using namespace std;

namespace two {

  class Transform {
    private:
      Transform* parent;
      vector<Transform*> children;

      Vec2 localPosition;
      float localRotation;
      Vec2 localScale;
      Vec2 globalPosition;
      float globalRotation;
      Vec2 globalScale;

      // basis vectors
      Vec2 right;
      Vec2 down;

      Uint32 entityId;

      void addChild(Transform* child);
      void removeChild(int childId);
      void removeChild(Transform* child);
      void updateLocal();
      void updateGlobal();
      void updateBasesAndChildren();

    public:
      Transform(Vec2 position = Vec2s::Zero,
                float rotation = 0.0f,
                Vec2 scale = Vec2s::One,
                Transform* _parent = NULL);
      ~Transform();

      Vec2 getLocalPosition();
      float getLocalRotation();
      Vec2 getLocalScale();
      Vec2 getPosition();
      float getRotation();
      Vec2 getScale();

      void setLocalPosition(Vec2 position);
      void setLocalRotation(float rotation);
      void setLocalScale(Vec2 scale);
      void setPosition(Vec2 position);
      void setRotation(float rotation);
      void setScale(Vec2 scale);

      void moveLocalPosition(Vec2 offset);
      void movePosition(Vec2 offset);
      void rotateLocalBy(float angle);
      void rotateBy(float angle);

      Transform* getParent();
      void setParent(Transform* newParent);
      Uint32 getEntityId();
      void setEntityId(Uint32 newId);

      vector<Transform*>& getChildren();
      Transform* getChild(int childId);
      size_t getNumChildren();

      Vec2 getRight();
      Vec2 getDown();
  };
}