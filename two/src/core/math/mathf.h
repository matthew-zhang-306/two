#pragma once

namespace two {
  class Mathf {
    public:
      const static float Pi;
      const static float Deg2Rad;
      const static float Rad2Deg;
      const static float Infinity;

      static float clamp(float value, float min, float max);
      static float lerp(float from, float to, float t);
      static float lerpUnclamped(float from, float to, float t);
      static float moveToward(float start, float target, float delta);

      static float sign(float value); // cannot return 0
      static int sign(int number); // cannot return 0
      static float signum(float value); // can return 0
      static int signum(int number); // can return 0
  };
}