#pragma once
#include "color.h"
#include <sstream>

using namespace std;
using namespace two;

/*
* Implementations for rgb/hsv conversions were
* lifted from the source code of Godot.
*/


Color::Color() {
  r = 0.0f;
  g = 0.0f;
  b = 0.0f;
  a = 1.0f;
}

Color::Color(float _r, float _g, float _b, float _a) {
  r = _r;
  g = _g;
  b = _b;
  a = _a;
}

Color Color::fromHSV(float h, float s, float v, float _a) {
	if (s == 0.0f) {
		// achromatic
		return Color(v, v, v, _a);
	}

	h *= 6.0f;
	h = fmod(h, 6.0f);
	int i = (int)floor(h);
	float f = h - i;

	float p = v * (1.0f - s);
	float q = v * (1.0f - s * f);
	float t = v * (1.0f - s * (1.0f - f));

	switch (i) {
	case 0:
		// red to yellow
		return Color(v, t, p, _a);
	case 1:
		// yellow to green
		return Color(q, v, p, _a);
	case 2:
		// green to cyan
		return Color(p, v, t, _a);
	case 3:
		// cyan to blue
		return Color(p, q, v, _a);
	case 4:
		// blue to magenta
		return Color(t, p, v, _a);
	default:
		// magenta to red
		return Color(v, p, q, _a);
	}
}


float Color::hue() const {
	float lo = min(min(r, g), b);
	float hi = max(max(r, g), b);

	if (lo == hi) {
		// achromatic
		return 0.0f;
	}

	float h;
	if (r == hi)
		h = (g - b) / (hi - lo); // between yellow & magenta
	else if (g == hi)
		h = 2.0f + (b - r) / (hi - lo); // between cyan & yellow
	else
		h = 4.0f + (r - g) / (hi - lo); // between magenta & cyan

	h /= 6.0f;
	if (h < 0.0f)
		h += 1.0f;

	return h;
}

float Color::sat() const {
	float lo = min(min(r, g), b);
	float hi = max(max(r, g), b);
	return hi != 0.0f ? (hi - lo) / hi : 0.0f;
}

float Color::val() const {
	return max(max(r, g), b);
}


Color Color::withRed(float _r) const {
	return Color(_r, g, b, a);
}

Color Color::withGreen(float _g) const {
	return Color(r, _g, b, a);
}

Color Color::withBlue(float _b) const {
	return Color(r, g, _b, a);
}

Color Color::withAlpha(float _a) const {
	return Color(r, g, b, _a);
}

Color Color::withHue(float h) const {
	return Color::fromHSV(h, sat(), val());
}

Color Color::withSat(float s) const {
	return Color::fromHSV(hue(), s, val());
}

Color Color::withVal(float v) const {
	return Color::fromHSV(hue(), sat(), v);
}

Color Color::darken(float amount) const {
	amount = min(max(amount, 0.0f), 1.0f);
	return Color(r * amount, g * amount, b * amount, a);
}

Color Color::lighten(float amount) const {
	amount = min(max(amount, 0.0f), 1.0f);
	return Color(
		r * (1 - amount) + amount,
		g * (1 - amount) + amount,
		b * (1 - amount) + amount,
		a
	);
}


bool Color::operator==(Color rhs) {
	return
		r == rhs.r &&
		g == rhs.g &&
		b == rhs.b &&
		a == rhs.a;
}

bool Color::operator!=(Color rhs) {
	return !(*this == rhs);
}

string Color::toString() const {
	ostringstream ss;
	ss << "Color<" << r << ", " << g << ", " << b << ", " << a << ">";
	return ss.str();
}

SDL_Color Color::toSDL() const {
	SDL_Color sdlColor;
	sdlColor.r = (Uint8)round(r * 255.0f);
	sdlColor.g = (Uint8)round(g * 255.0f);
	sdlColor.b = (Uint8)round(b * 255.0f);
	sdlColor.a = (Uint8)round(a * 255.0f);
	return sdlColor;
}
