#include "rect2.h"
#include <sstream>

using namespace std;
using namespace two;

Rect2::Rect2() {
  x = 0;
  y = 0;
  w = 0;
  h = 0;
}

Rect2::Rect2(float _x, float _y, float _w, float _h) {
  x = _x;
  y = _y;
  w = _w;
  h = _h;
}

Rect2::Rect2(Vec2 pos, Vec2 size, Vec2 anchor) {
  anchor = clampAnchor(anchor);

  x = pos.x - anchor.x * size.x;
  y = pos.y - anchor.y * size.y;
  w = size.x;
  h = size.y;
}

Rect2 Rect2::fromMinMax(Vec2 min, Vec2 max) {
  return Rect2(min, max - min);
}

Rect2 Rect2::fromCenter(Vec2 center, Vec2 size) {
  return Rect2(center, size, Vec2s::Center);
}


float Rect2::left() {
  return x;
}

float Rect2::right() {
  return x + w;
}

float Rect2::top() {
  return y;
}

float Rect2::bottom() {
  return y + h;
}

Vec2 Rect2::topLeft() {
  return Vec2(left(), top());
}

Vec2 Rect2::topRight() {
  return Vec2(right(), top());
}

Vec2 Rect2::bottomLeft() {
  return Vec2(left(), bottom());
}

Vec2 Rect2::bottomRight() {
  return Vec2(right(), bottom());
}



Vec2 Rect2::pos(Vec2 anchor) {
  anchor = clampAnchor(anchor);
  return Vec2(x, y) + size() * anchor;
}

Vec2 Rect2::min() {
  return Vec2(x, y);
}

Vec2 Rect2::center() {
  return Vec2(x + w/2.0f, y + h/2.0f);
}

Vec2 Rect2::max() {
  return Vec2(x + w, y + h);
}

Vec2 Rect2::size() {
  return Vec2(w, h);
}

Vec2 Rect2::extents() {
  return size() / 2.0f;
}

Rect2 Rect2::withPos(Vec2 pos, Vec2 anchor) {
  anchor = clampAnchor(anchor);
  return Rect2(pos, size(), anchor);
}

Rect2 Rect2::withSize(Vec2 size, Vec2 anchor) {
  anchor = clampAnchor(anchor);
  return Rect2(pos(anchor), size, anchor);
}

Rect2 Rect2::translated(Vec2 offset) {
  return Rect2(x + offset.x, y + offset.y, w, h);
}


float Rect2::area() {
  return w * h;
}

bool Rect2::contains(Vec2 point) {
  return point.x >= min().x && point.y >= min().y
    && point.x <= max().x && point.y <= max().y;
}

bool Rect2::intersects(Rect2 other) {
  return
    other.min().x < max().x &&
    other.max().x > min().x &&
    other.min().y < max().y &&
    other.max().y > min().y;
}

bool Rect2::contains(Rect2 other) {
  return contains(other.min()) && contains(other.max());
}

Rect2 Rect2::overlap(Rect2 other) {
  if (!intersects(other))
    return Rect2s::Zero;
  return Rect2::fromMinMax(
    Vec2(std::max(min().x, other.min().x), std::max(min().y, other.min().y)),
    Vec2(std::min(max().x, other.max().x), std::min(max().y, other.max().y))
  );
}

Rect2 Rect2::combine(Rect2 other) {
  return Rect2::fromMinMax(
    Vec2(std::min(min().x, other.min().x), std::min(min().y, other.min().y)),
    Vec2(std::max(max().x, other.max().x), std::max(max().y, other.max().y))
  );
}

Rect2 Rect2::inflate(float amount) {
  return inflate(Vec2(2 * amount, 2 * amount));
}

Rect2 Rect2::inflate(Vec2 amount) {
  return Rect2::fromCenter(center(), size() + amount);
}


bool Rect2::operator==(Rect2 rhs) {
  return
    x == rhs.x &&
    y == rhs.y &&
    w == rhs.w &&
    h == rhs.h;
}

bool Rect2::operator!=(Rect2 rhs) {
  return !(*this == rhs);
}

string Rect2::toString() {
  ostringstream ss;
  ss << "Rect2[" << x << ", " << y << ", " << w << ", " << h << "]";
  return ss.str();
}

SDL_Rect Rect2::toSDL() {
  SDL_Rect sdlRect;
  sdlRect.x = (int)round(x);
  sdlRect.y = (int)round(y);
  sdlRect.w = (int)round(x + w) - sdlRect.x;
  sdlRect.h = (int)round(y + h) - sdlRect.y;
  return sdlRect;
}


Vec2 Rect2::clampAnchor(Vec2 anchor) {
  // TODO: replace calls to std::min/std::max with own math utilities (with min, max, clamp01, etc)
  return Vec2(
    std::min(std::max(anchor.x, 0.0f), 1.0f),
    std::min(std::max(anchor.y, 0.0f), 1.0f)
  );
}