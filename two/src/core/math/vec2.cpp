#include "vec2.h"
#include "mathf.h"
#include <sstream>

using namespace std;
using namespace two;

Vec2::Vec2() {
  x = 0;
  y = 0;
}

Vec2::Vec2(float _x, float _y) {
  x = _x;
  y = _y;
}

Vec2 Vec2::fromAngle(float angle, float magnitude) {
  float rad = Mathf::Deg2Rad * angle;
  return Vec2(cos(rad), sin(rad)) * magnitude;
}


bool Vec2::operator==(Vec2 rhs) {
  return x == rhs.x && y == rhs.y;
}

bool Vec2::operator!=(Vec2 rhs) {
  return !(*this == rhs);
}


Vec2 Vec2::operator+() {
  return *this;
}

Vec2 Vec2::operator-() {
  return Vec2(-x, -y);
}

Vec2 Vec2::operator+(Vec2 rhs) {
  return Vec2(x + rhs.x, y + rhs.y);
}

Vec2 Vec2::operator-(Vec2 rhs) {
  return Vec2(x - rhs.x, y - rhs.y);
}

Vec2 Vec2::operator*(Vec2 rhs) {
  return Vec2(x * rhs.x, y * rhs.y);
}

Vec2 Vec2::operator/(Vec2 rhs) {
  return Vec2(x / rhs.x, y / rhs.y);
}

Vec2 Vec2::operator/(float rhs) {
  return Vec2(x / rhs, y / rhs);
}



Vec2::operator string() {
  return toString();
}

string Vec2::toString() const {
  ostringstream ss;
  ss << "Vec2(" << x << ", " << y << ")";
  return ss.str();
}


Vec2 Vec2::normalized() const {
  if (sqrMagnitude() == 0) {
    return *this;
  }
  
  return Vec2(x, y) / magnitude();
}

float Vec2::magnitude() const {
  return sqrt(x * x + y * y);
}

float Vec2::sqrMagnitude() const {
  return x * x + y * y;
}

float Vec2::angle(Vec2 from) const {
  float rad = atan2(y * from.x - x * from.y, x * from.x + y * from.y);
  return Mathf::Rad2Deg * rad;
}

Vec2 Vec2::rotated(float angle) const {
  float rad = Mathf::Deg2Rad * angle;
  return Vec2(x * cos(rad) - y * sin(rad), x * sin(rad) + y * cos(rad));
}

float Vec2::dot(Vec2 other) const {
  return x * other.x + y * other.y;
}

Vec2 Vec2::projected(Vec2 axis) const {
  return (dot(axis) / axis.sqrMagnitude()) * axis;
}

Vec2 Vec2::perpProjected(Vec2 axis) const {
  return Vec2(x, y) - projected(axis);
}


Vec2 Vec2::lerp(Vec2 from, Vec2 to, float t) {
  return Vec2(
    Mathf::lerp(from.x, to.x, t),
    Mathf::lerp(from.y, to.y, t)
  );
}

Vec2 Vec2::lerpUnclamped(Vec2 from, Vec2 to, float t) {
  return Vec2(
    Mathf::lerpUnclamped(from.x, to.x, t),
    Mathf::lerpUnclamped(from.y, to.y, t)
  );
}


SDL_Point Vec2::toSDLPoint() const {
  return {(int)round(x), (int)round(y)};
}