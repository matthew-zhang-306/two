#pragma once
#include <string>
#include <SDL.h>

using namespace std;

namespace two {
  struct Color {
    public:
      float r;
      float g;
      float b;
      float a;

      Color();
      Color(float _r, float _g, float _b, float _a = 1.0f);
      static Color fromHSV(float h, float s, float v, float _a = 1.0f);

      float hue() const;
      float sat() const;
      float val() const;

      Color withRed(float _r) const;
      Color withGreen(float _g) const;
      Color withBlue(float _b) const;
      Color withAlpha(float _a) const;
      Color withHue(float h) const;
      Color withSat(float s) const;
      Color withVal(float v) const;
      Color darken(float amount) const;
      Color lighten(float amount) const;

      bool operator==(Color rhs);
      bool operator!=(Color rhs);
      string toString() const;
      SDL_Color toSDL() const;

      friend string operator+(Color color, string s) {
        return color.toString() + s;
      }
      friend string operator+(string s, Color color) {
        return s + color.toString();
      }
      friend ostream& operator<<(ostream& o, Color& color) {
        return o << color.toString();
      }
      friend string operator<<(string s, Color color) {
        return s + color;
      }
  };


  class Colors {
    public:
      const static Color White     ;
      const static Color LightGray ;
      const static Color DarkGray  ;
      const static Color Black     ;
      const static Color WhiteTrans;
      const static Color BlackTrans;
      const static Color Red       ;
      const static Color Orange    ;
      const static Color Yellow    ;
      const static Color Green     ;
      const static Color Cyan      ;
      const static Color Blue      ;
      const static Color Purple    ;
      const static Color Magenta   ;
  };

  inline const Color Colors::White       = Color(1.0f, 1.0f, 1.0f, 1.0f);
  inline const Color Colors::LightGray   = Color(0.7f, 0.7f, 0.7f, 1.0f);
  inline const Color Colors::DarkGray    = Color(0.3f, 0.3f, 0.3f, 1.0f);
  inline const Color Colors::Black       = Color(0.0f, 0.0f, 0.0f, 1.0f);
  inline const Color Colors::WhiteTrans  = Color(1.0f, 1.0f, 1.0f, 0.0f);
  inline const Color Colors::BlackTrans  = Color(0.0f, 0.0f, 0.0f, 0.0f);
  inline const Color Colors::Red         = Color(1.0f, 0.0f, 0.0f, 1.0f);
  inline const Color Colors::Orange      = Color(1.0f, 0.5f, 0.0f, 1.0f);
  inline const Color Colors::Yellow      = Color(1.0f, 1.0f, 0.0f, 1.0f);
  inline const Color Colors::Green       = Color(0.0f, 1.0f, 0.0f, 1.0f);
  inline const Color Colors::Cyan        = Color(0.0f, 1.0f, 1.0f, 1.0f);
  inline const Color Colors::Blue        = Color(0.0f, 0.0f, 1.0f, 1.0f);
  inline const Color Colors::Purple      = Color(0.5f, 0.0f, 1.0f, 1.0f);
  inline const Color Colors::Magenta     = Color(1.0f, 0.0f, 1.0f, 1.0f);
}