#include "transform.h"
#include "../debug/debug.h"

using namespace std;
using namespace two;


Transform::Transform(Vec2 position, float rotation, Vec2 scale, Transform* _parent) {
  parent = _parent;
  setPosition(position);
  setRotation(rotation);
  setScale(scale);
}

Transform::~Transform() {
  if (parent != NULL) {
    // remove parent so that the parent knows this transform is going away
    setParent(NULL);
  }
}


Vec2 Transform::getLocalPosition() {
  return localPosition;
}

float Transform::getLocalRotation() {
  return localRotation;
}

Vec2 Transform::getLocalScale() {
  return localScale;
}

Vec2 Transform::getPosition() {
  return globalPosition;
}

float Transform::getRotation() {
  return globalRotation;
}

Vec2 Transform::getScale() {
  return globalScale;
}


void Transform::setLocalPosition(Vec2 position) {
  localPosition = position;
  updateGlobal();
  updateBasesAndChildren();
}

void Transform::setLocalRotation(float rotation) {
  localRotation = rotation;
  updateGlobal();
  updateBasesAndChildren();
}

void Transform::setLocalScale(Vec2 scale) {
  localScale = scale;
  updateGlobal();
  updateBasesAndChildren();
}

void Transform::setPosition(Vec2 position) {
  globalPosition = position;
  updateLocal();
  updateBasesAndChildren();
}

void Transform::setRotation(float rotation) {
  globalRotation = rotation;
  updateLocal();
  updateBasesAndChildren();
}

void Transform::setScale(Vec2 scale) {
  globalScale = scale;
  updateLocal();
  updateBasesAndChildren();
}

void Transform::moveLocalPosition(Vec2 offset) {
  if (offset != Vec2s::Zero) {
    setLocalPosition(getLocalPosition() + offset);
  }
}

void Transform::movePosition(Vec2 offset) {
  if (offset != Vec2s::Zero) {
    setPosition(getPosition() + offset);
  }
}

void Transform::rotateLocalBy(float angle) {
  if (fmod(angle, 360.0f) != 0.0f) {
    setLocalRotation(getLocalRotation() + angle);
  }
}

void Transform::rotateBy(float angle) {
  if (fmod(angle, 360.0f) != 0.0f) {
    setRotation(getRotation() + angle);
  }
}



void Transform::updateLocal() {
  if (parent == NULL) {
    localPosition = globalPosition;
    localRotation = globalRotation;
    localScale = globalScale;
  }
  else {
    auto globalPosOffset = globalPosition - parent->globalPosition;
    localPosition = Vec2(
      globalPosOffset.dot(parent->right) / parent->localScale.x,
      globalPosOffset.dot(parent->down) / parent->localScale.y);
    localRotation = globalRotation - parent->globalRotation;
    localScale = Vec2(
      parent->globalScale.x == 0 ? 0 : globalScale.x / parent->globalScale.x,
      parent->globalScale.y == 0 ? 0 : globalScale.y / parent->globalScale.y);
  }
}

void Transform::updateGlobal() {
  if (parent == NULL) {
    globalPosition = localPosition;
    globalRotation = localRotation;
    globalScale = localScale;
  }
  else {
    globalPosition = parent->globalPosition + (parent->right * localPosition.x + parent->down * localPosition.y);
    globalRotation = parent->globalRotation + localRotation;
    globalScale = parent->globalScale * localScale;
  }
}

void Transform::updateBasesAndChildren() {
  right = Vec2::fromAngle(globalRotation, globalScale.x);
  down = Vec2::fromAngle(globalRotation + 90.0f, globalScale.y);

  for (Transform* child : children) {
    if (child == NULL) {
      Debug::error("Transform::updateBasesAndChildren", "a transform with id " + Debug::u(entityId) + " has a null child");
    }
    child->updateGlobal();
    child->updateBasesAndChildren();
  }
}


Transform* Transform::getParent() {
  return parent;
}

void Transform::setParent(Transform* newParent) {
  if (parent != NULL) {
    parent->removeChild(this);
  }
  if (newParent != NULL) {
    newParent->addChild(this);
  }

  parent = newParent;
  updateLocal();
  // don't need to update bases and children because our global transform hasn't changed
}

Uint32 Transform::getEntityId() {
  return entityId;
}

void Transform::setEntityId(Uint32 newId) {
  entityId = newId;
}


void Transform::addChild(Transform* child) {
  children.push_back(child);
}

void Transform::removeChild(int childId) {
  children.erase(children.begin() + childId);
}

void Transform::removeChild(Transform* child) {
  auto f = find(children.begin(), children.end(), child);
  if (f == children.end()) {
    Debug::error("Transform::removeChild", "transform " + Debug::u(entityId) + " does not have child " + Debug::u(child->entityId));
    return;
  }

  children.erase(f);
}

vector<Transform*>& Transform::getChildren() {
  return children;
}

Transform* Transform::getChild(int childId) {
  return children[childId];
}

size_t Transform::getNumChildren() {
  return children.size();
}


Vec2 Transform::getRight() {
  return right;
}

Vec2 Transform::getDown() {
  return down; 
}
