#pragma once
#include <string>
#include <limits>
#include <SDL.h>
#include "vec2.h"

using namespace std;

namespace two {
  struct Rect2 {
    public:
      float x;
      float y;
      float w;
      float h;

      Rect2();
      Rect2(float _x, float _y, float _w, float _h);
      Rect2(Vec2 pos, Vec2 size, Vec2 anchor = Vec2s::TopLeft);
      static Rect2 fromMinMax(Vec2 min, Vec2 max);
      static Rect2 fromCenter(Vec2 center, Vec2 size);

      float left();
      float right();
      float top();
      float bottom();
      Vec2 topLeft();
      Vec2 topRight();
      Vec2 bottomLeft();
      Vec2 bottomRight();

      Vec2 pos(Vec2 anchor = Vec2s::TopLeft);
      Vec2 min();
      Vec2 center();
      Vec2 max();
      Vec2 size();
      Vec2 extents();
      Rect2 withPos(Vec2 pos, Vec2 anchor = Vec2s::TopLeft);
      Rect2 withSize(Vec2 size, Vec2 anchor = Vec2s::TopLeft);
      Rect2 translated(Vec2 offset);
      
      float area();
      bool contains(Vec2 point);
      bool intersects(Rect2 other);
      bool contains(Rect2 other);
      Rect2 overlap(Rect2 other);
      Rect2 combine(Rect2 other);

      // inflate with float argument expands all sides outward by the amount
      Rect2 inflate(float amount);
      // inflate with vec argument adds the amount to the current size
      Rect2 inflate(Vec2 amount);

      bool operator==(Rect2 rhs);
      bool operator!=(Rect2 rhs);
      string toString();
      SDL_Rect toSDL();

      friend string operator+(Rect2 rect, string s) {
        return rect.toString() + s;
      }
      friend string operator+(string s, Rect2 rect) {
        return s + rect.toString();
      }
      friend ostream& operator<<(ostream& o, Rect2& rect) {
        return o << rect.toString();
      }
      friend string operator<<(string s, Rect2 rect) {
        return s + rect;
      }

    private:
      Vec2 clampAnchor(Vec2 anchor);
  };


  class Rect2s {
    public:
      const static Rect2 Zero ;
      const static Rect2 Unit ;
      const static Rect2 Empty;
  };

  inline const Rect2 Rect2s::Zero  = Rect2(0, 0, 0, 0);
  inline const Rect2 Rect2s::Unit  = Rect2(0, 0, 1, 1);
  inline const Rect2 Rect2s::Empty = Rect2(Vec2s::Inf, Vec2s::Inf);
}