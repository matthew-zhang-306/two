#pragma once
#include <sstream>
#include <string>
#include <limits>
#include <SDL.h>
#include "mathf.h"

using namespace std;

namespace two {

  struct Vec2 {
    public:
      float x;
      float y;

      Vec2();
      Vec2(float _x, float _y);
      static Vec2 fromAngle(float angle, float magnitude = 1.0f);

      bool operator==(Vec2 rhs);
      bool operator!=(Vec2 rhs);

      Vec2 operator+();
      Vec2 operator-();
      Vec2 operator+(Vec2 rhs);
      Vec2 operator-(Vec2 rhs);
      Vec2 operator*(Vec2 rhs);
      Vec2 operator/(Vec2 rhs);
      Vec2 operator/(float rhs);

      friend Vec2 operator*(Vec2 lhs, float rhs) {
        return Vec2(lhs.x * rhs, lhs.y * rhs);
      }
      friend Vec2 operator*(float lhs, Vec2 rhs) {
        return rhs * lhs;
      }

      friend string operator+(Vec2 vec, string s) {
        return vec.toString() + s;
      }
      friend string operator+(string s, Vec2 vec) {
        return s + vec.toString();
      }
      friend ostream& operator<<(ostream& o, Vec2& vec) {
        return o << vec.toString();
      }
      friend string operator<<(string s, Vec2 vec) {
        return s + vec;
      }
      operator string();
      string toString() const;

      Vec2 normalized() const;
      float magnitude() const;
      float sqrMagnitude() const;
      float angle(Vec2 from) const;
      Vec2 rotated(float angle) const;
      float dot(Vec2 other) const;
      Vec2 projected(Vec2 axis) const;
      Vec2 perpProjected(Vec2 axis) const;
      
      static Vec2 lerp(Vec2 from, Vec2 to, float t);
      static Vec2 lerpUnclamped(Vec2 from, Vec2 to, float t);

      SDL_Point toSDLPoint() const;
  };


  class Vec2s {
    public:
      const static Vec2 Zero  ;
      const static Vec2 One   ;
      const static Vec2 Half  ;
      const static Vec2 Left  ;
      const static Vec2 Right ;
      const static Vec2 Up    ;
      const static Vec2 Down  ;
      const static Vec2 Inf   ;
      const static Vec2 NegInf;

      const static Vec2 TopLeft     ;
      const static Vec2 TopMid      ;
      const static Vec2 TopRight    ;
      const static Vec2 MidLeft     ;
      const static Vec2 Center      ;
      const static Vec2 MidRight    ;
      const static Vec2 BottomLeft  ;
      const static Vec2 BottomMid   ;
      const static Vec2 BottomRight ;
  };

  inline const Vec2 Vec2s::Zero   = Vec2( 0.0f,  0.0f);
  inline const Vec2 Vec2s::One    = Vec2( 1.0f,  1.0f);
  inline const Vec2 Vec2s::Half   = Vec2( 0.5f,  0.5f);
  inline const Vec2 Vec2s::Left   = Vec2(-1.0f,  0.0f);
  inline const Vec2 Vec2s::Right  = Vec2( 1.0f,  0.0f); 
  inline const Vec2 Vec2s::Up     = Vec2( 0.0f, -1.0f);
  inline const Vec2 Vec2s::Down   = Vec2( 0.0f,  1.0f);
  inline const Vec2 Vec2s::Inf    = Vec2( Mathf::Infinity,  Mathf::Infinity);
  inline const Vec2 Vec2s::NegInf = Vec2(-Mathf::Infinity, -Mathf::Infinity);

  inline const Vec2 Vec2s::TopLeft     = Vec2(0.0f, 0.0f);
  inline const Vec2 Vec2s::TopMid      = Vec2(0.5f, 0.0f);
  inline const Vec2 Vec2s::TopRight    = Vec2(1.0f, 0.0f);
  inline const Vec2 Vec2s::MidLeft     = Vec2(0.0f, 0.5f);
  inline const Vec2 Vec2s::Center      = Vec2(0.5f, 0.5f);
  inline const Vec2 Vec2s::MidRight    = Vec2(1.0f, 0.5f);
  inline const Vec2 Vec2s::BottomLeft  = Vec2(0.0f, 1.0f);
  inline const Vec2 Vec2s::BottomMid   = Vec2(0.5f, 1.0f);
  inline const Vec2 Vec2s::BottomRight = Vec2(1.0f, 1.0f);

}