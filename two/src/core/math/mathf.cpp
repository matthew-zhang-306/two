#include "mathf.h"
#include <algorithm>
#include <cmath>

using namespace two;
using namespace std;

const float Mathf::Pi = 3.141592653589793238462643383279502884f;
const float Mathf::Deg2Rad = Mathf::Pi / 180.0f;
const float Mathf::Rad2Deg = 180.0f / Mathf::Pi;
const float Mathf::Infinity = numeric_limits<float>::infinity();

float Mathf::clamp(float value, float min, float max) {
  return std::min(std::max(value, min), max);
}

float Mathf::lerp(float from, float to, float t) {
  return lerpUnclamped(from, to, clamp(t, 0.0f, 1.0f));
}

float Mathf::lerpUnclamped(float from, float to, float t) {
  return from * (1 - t) + to * t;
}

float Mathf::moveToward(float start, float target, float delta) {
  float distance = abs(target - start);
  if (distance < delta) {
    return target;
  }
  else {
    return start + sign(target - start) * delta;
  }
}


float Mathf::sign(float value) {
  return value >= 0.0f ? 1.0f : -1.0f;
}

int Mathf::sign(int number) {
  return number >= 0 ? 1 : -1;
}

float Mathf::signum(float value) {
  return value == 0.0f ? 0.0f : sign(value);
}

int Mathf::signum(int number) {
  return number == 0 ? 0 : sign(number);
}