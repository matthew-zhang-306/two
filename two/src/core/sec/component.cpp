#include "component.h"
#include "entity.h"
#include "scene.h"
#include <iostream>
#include <numeric>

using namespace std;
using namespace two;

Uint32 cID::nextId = 0;


bool ComponentDef::compare(const ComponentDef& a, const ComponentDef& b) {
  if (a.updateOrder != b.updateOrder) {
    return a.updateOrder < b.updateOrder;
  }
  return a.componentId < b.componentId;
}

bool Component::compareByDrawOrder(const Component* a, const Component* b) {
  if (a->sortOrder != b->sortOrder) {
    return a->sortOrder < b->sortOrder;
  }
  return a->entityId < b->entityId;
}


Component::Component() {
  entityId = numeric_limits<Uint32>::max();
  scene = NULL;
  isEnabled = false;
  sortOrder = 0;
}

void Component::initComponent(Uint32 _entityId, Scene* _scene) {
  entityId = _entityId;
  scene = _scene;
  isEnabled = true;
}

Component::~Component() {

}


Entity Component::getEntity() {
  return scene->getEntity(entityId);
}

Scene* Component::getScene() {
  return scene;
}

bool Component::getEnabled() {
  return isEnabled;
}

void Component::setEnabled(bool enabled) {
  isEnabled = enabled;
}

Transform* Component::getTransform() {
  return getEntity().getTransform();
}


int Component::getSortOrder() {
  return sortOrder;
}

void Component::setSortOrder(int newSortOrder) {
  bool didRemove = getScene()->removeRenderable(this);
  sortOrder = newSortOrder;

  if (didRemove) {
    getScene()->addRenderable(this);
  }
}


void Component::onStart() {}
void Component::onDestroy() {}
void Component::update(float dt) {}
void Component::draw(Renderer* renderer) {}
