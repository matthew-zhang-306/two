#include "entity.h"
#include "component.h"
#include "scene.h"
#include <iostream>
#include <numeric>

using namespace std;
using namespace two;

Entity::Entity() {
  id = numeric_limits<Uint32>::max();
  scene = NULL;
  isEnabledSelf = false;
  entityState = Entity::state::dead;
}

Entity::Entity(Uint32 _id, Scene* _scene) {
  id = _id;
  scene = _scene;
  isEnabledSelf = true;
  entityState = Entity::state::born;
}

Entity::~Entity() {

}


Uint32 Entity::getId() {
  return id;
}

Entity::state Entity::getState() {
  return entityState;
}

void Entity::setState(Entity::state newState) {
  entityState = newState;
}

Scene* Entity::getScene() {
  return scene;
}

Transform* Entity::getTransform() {
  return scene->getTransform(id);
}


bool Entity::getEnabledSelf() {
  return isEnabledSelf;
}

bool Entity::getEnabledInTree() {
  // TODO: get working with scene tree
  return isEnabledSelf;
}

void Entity::setEnabled(bool enabled) {
  isEnabledSelf = enabled;
}

void Entity::destroy() {
  // destroy children first
  for (Transform* t : getTransform()->getChildren()) {
    scene->getEntity(t->getEntityId()).destroy();
  }

  scene->removeEntity(id);
}
