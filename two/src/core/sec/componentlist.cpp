#include "componentlist.h"
#include <iostream>

using namespace std;
using namespace two;

ComponentList::ComponentList() {

}

ComponentList::~ComponentList() {

}


int ComponentList::getSize() {
  return components.size();
}

void ComponentList::clear() {
  components.clear();
}


bool ComponentList::set(Uint32 entityId, Component* component) {
  if (has(entityId)) {
    // component already exists.
    Debug::error("ComponentList::set", "can't add component to entity " + Debug::u(entityId));
    return false;
  }

  components.push_back(component);
  entityIdToIndex.emplace(entityId, components.size() - 1);
  indexToEntityId.emplace(components.size() - 1, entityId);
  
  return true;
}

Component* ComponentList::remove(Uint32 entityId) {
  if (!has(entityId)) {
    // component does not exist.
    Debug::error("ComponentList::remove", "can't remove component from entity " + Debug::u(entityId));
    return NULL;
  }
  
  // fill this slot with the pointer at the back
  Uint32 oldIndex = entityIdToIndex[entityId];
  Uint32 lastEntityId = indexToEntityId[components.size() - 1];
  Component* removed = components[oldIndex];
  components[oldIndex] = components[components.size() - 1];
  entityIdToIndex[lastEntityId] = oldIndex;
  indexToEntityId[oldIndex] = lastEntityId;
  entityIdToIndex.erase(entityId);
  indexToEntityId.erase(lastEntityId);
  
  components.erase(components.end() - 1);
  return removed;
}

bool ComponentList::has(Uint32 entityId) {
  return entityIdToIndex.find(entityId) != entityIdToIndex.end();
}

Component* ComponentList::get(Uint32 entityId) {
  if (!has(entityId)) {
    // component does not exist.
    Debug::error("ComponentList::remove", "can't get component from entity " + Debug::u(entityId));
    return NULL;
  }

  return components[entityIdToIndex[entityId]];
}

vector<Component*>& ComponentList::getComponents() {
  return components;
}

