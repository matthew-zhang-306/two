#pragma once
#include <deque>
#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <vector>
#include <SDL.h>
#include "component.h"
#include "componentlist.h"
#include "../debug/debug.h"
#include "../rendering/rendering.h"

using namespace std;

namespace two {
  
  class Core;
  class Physics;
  class Entity;

  typedef map<ComponentDef, ComponentList*, CDefCompare> CListMap;
  typedef set<Component*, CDrawCompare> CRenderableSet;

  class Scene {
    private:
      struct NewComponent {
        public:
          Component* component;
          ComponentDef componentDef;
          Uint32 entityId;
          NewComponent(Component* c, ComponentDef d, Uint32 e) :
            component(c), componentDef(d), entityId(e) {};
      };

      Camera camera;
      
      vector<Entity> entities;
      vector<Entity> entitiesToAdd;
      vector<Entity> entitiesToRemove;
      deque<Uint32> freeIds;
      Uint32 numEntities;

      vector<Transform*> transforms;

      CListMap componentLists;
      CRenderableSet renderableComponents;
      vector<NewComponent> componentsToAdd;

      bool addComponentToLists(const NewComponent& newComponent);

    protected:
      Physics* physics;
      Core* core;

    public:
      Scene();
      virtual ~Scene();

      Core* getCore();
      Camera* getCamera();
      Physics* getPhysics();

      virtual void load(Core* _core);
      virtual void unload();

      Entity createEntity();
      void removeEntity(Uint32 entityId);
      bool hasEntity(Uint32 entityId);
      Entity getEntity(Uint32 entityId);

      Transform* getTransform(Uint32 entityId);

      template <typename TComponent, typename ...TArgs>
        TComponent* createComponent(Uint32 entityId, TArgs&& ...args);
      // can't remove a component individually. should disable it instead
      template <typename TComponent>
        bool hasComponent(Uint32 entityId);
      template <typename TComponent>
        TComponent* getComponent(Uint32 entityId);

      bool addRenderable(Component* component);
      bool removeRenderable(Component* component);

      void doECLifeCycle();
      void update(float dt);
      void draw();
  };



  template <typename TComponent, typename ...TArgs>
  TComponent* Scene::createComponent(Uint32 entityId, TArgs&& ...args) {
    if (!hasEntity(entityId)) {
      Debug::error("Scene::addComponent", "trying to add component to nonexistant entity " + Debug::u(entityId));
      return NULL;
    }

    // create component
    TComponent* newTComponent = new TComponent(forward<TArgs>(args)...);
    Component* newComponent = static_cast<Component*>(newTComponent);

    newComponent->initComponent(entityId, this);
    ComponentDef def = Component::getComponentDef<TComponent>();
    componentsToAdd.emplace_back(newComponent, def, entityId);
    
    return newTComponent;
  }

  template <typename TComponent>
  bool Scene::hasComponent(Uint32 entityId) {
    if (!hasEntity(entityId))
      return false;

    ComponentDef def = Component::getComponentDef<TComponent>();
    auto pair = componentLists.find(def);
    return pair != componentLists.end() &&
      pair->second->has(entityId);
  }

  template <typename TComponent>
  TComponent* Scene::getComponent(Uint32 entityId) {
    if (!hasComponent<TComponent>(entityId)) {
      Debug::error("Scene::getComponent", "entity " + Debug::u(entityId) + " does not have the desired component");
      return NULL;
    }

    ComponentDef def = Component::getComponentDef<TComponent>();
    return static_cast<TComponent*>(
      componentLists[def]->get(entityId)
    );
  }

}