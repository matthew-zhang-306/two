#include "scene.h"
#include "component.h"
#include "entity.h"
#include "../physics/physics.h"
#include "../core.h"
#include <iostream>

using namespace std;
using namespace two;

Scene::Scene() {
  core = NULL;
  physics = new Physics();

  numEntities = 0;
  componentLists = CListMap(&ComponentDef::compare);
  renderableComponents = CRenderableSet(&Component::compareByDrawOrder);
}

Scene::~Scene() {
  delete physics;

  // delete component lists
  for (auto pair : componentLists) {
    delete pair.second;
  }
}


Core* Scene::getCore() {
  return core;
}

Camera* Scene::getCamera() {
  return &camera;
}

Physics* Scene::getPhysics() {
  return physics;
}


void Scene::load(Core* _core) {
  core = _core;
  camera = Camera(Rect2(Vec2s::Zero, core->getRenderer()->getWindowSize()));
}

void Scene::unload() {
  for (Uint32 i = 0; i < numEntities; i++) {
    if (hasEntity(i)) {
      removeEntity(i);
    }
  }

  for (NewComponent component : componentsToAdd) {
    delete component.component;
  }

  entitiesToAdd.clear();
  componentsToAdd.clear();
}


Entity Scene::createEntity() {
  // get available id
  Uint32 id = numEntities;
  if (!freeIds.empty()) {
    id = freeIds.front();
    freeIds.pop_front();
  }

  Entity entity(id, this);
  Transform* transform = new Transform();
  transform->setEntityId(id);

  if (id == numEntities) {
    // need more space in the entities list
    entities.push_back(entity);
    transforms.push_back(transform);
  }
  else {
    entities[id] = entity;
    transforms[id] = transform;
  }

  entitiesToAdd.insert(entitiesToAdd.end(), entity);
  numEntities++;
  return entity;
}

void Scene::removeEntity(Uint32 entityId) {
  // check if this id is valid
  if (!hasEntity(entityId) || entities[entityId].getState() != Entity::state::alive) {
    Debug::error("Scene::removeEntity", "bad entity id " + Debug::u(entityId));
    return;
  }

  entities[entityId].setState(Entity::state::dying);
  entitiesToRemove.insert(entitiesToRemove.end(), entities[entityId]);
}

bool Scene::hasEntity(Uint32 entityId) {
  return entityId < entities.size() && entities[entityId].getState() != Entity::state::dead;
}

Entity Scene::getEntity(Uint32 entityId) {
  if (!hasEntity(entityId)) {
    Debug::error("Scene::getEntity", "bad entity id " + Debug::u(entityId));
    return Entity();
  }

  return entities[entityId];
}


Transform* Scene::getTransform(Uint32 entityId) {
  if (!hasEntity(entityId)) {
    Debug::error("Scene::getTransform", "bad entity id " + Debug::u(entityId));
    return NULL;
  }

  return transforms[entityId];
}


bool Scene::addComponentToLists(const NewComponent& newComponent) {
  // get correct component list
  /*
  if (newComponent.typeId > componentLists.size()) {
    // something is off
    Debug::warn("Scene::addComponentToList", "component id " + Debug::u(newComponent.typeId) + " is higher than we expect! we currently account for this but it shouldn't happen! size of lists = " + Debug::u(componentLists.size()));
  }
  */

  ComponentList* list = NULL;
  auto pair = componentLists.find(newComponent.componentDef);
  if (pair == componentLists.end()) {
    // make a new list
    list = new ComponentList();
    componentLists.emplace(newComponent.componentDef, list);
  }
  else {
    list = pair->second;
  }

  if (list->set(newComponent.entityId, newComponent.component)) {
    // add successful
    // is it a renderable?
    if (newComponent.componentDef.renderable) {
      renderableComponents.insert(newComponent.component);
    }
    return true;
  }
  return false;
}


bool Scene::addRenderable(Component* component) {
  auto result = renderableComponents.insert(component);
  return result.second;
}

bool Scene::removeRenderable(Component* component) {
  size_t result = renderableComponents.erase(component);
  return result > 0;
}


void Scene::doECLifeCycle() {
  // destroy entities, and their components
  for (Entity entity : entitiesToRemove) {
    // first delete all components
    for (auto pair : componentLists) {
      ComponentList* componentList = pair.second;
      if (componentList->has(entity.getId())) {
        // we call onDestroy on all components before deleting anything so that references exist
        Component* component = componentList->get(entity.getId());
        component->onDestroy();
      }
    }
    for (auto pair : componentLists) {
      ComponentList* componentList = pair.second;
      if (componentList->has(entity.getId())) {
        Component* component = componentList->remove(entity.getId());

        // remove it from the renderable set if it is renderable
        renderableComponents.erase(component);

        delete component;
      }
    }

    // then delete transform
    delete transforms[entity.getId()];
    transforms[entity.getId()] = NULL;

    // then recycle the entity
    entities[entity.getId()].setState(Entity::state::dead);
    freeIds.push_back(entity.getId());
    numEntities--;
  }
  entitiesToRemove.clear();

  // add new entities
  for (Entity entity : entitiesToAdd) {
    entities[entity.getId()] = entity;
    entities[entity.getId()].setState(Entity::state::alive);
  }
  entitiesToAdd.clear();

  // add new components
  for (const NewComponent& component : componentsToAdd) {
    addComponentToLists(component);
  }
  for (const NewComponent& component : componentsToAdd) {
    // we call onStart after adding everything so that all references are there
    component.component->onStart();
  }
  componentsToAdd.clear();
}

void Scene::update(float dt) {
  for (auto pair : componentLists) {
    ComponentList* componentList = pair.second;
    for (Component* component : componentList->getComponents()) {
      component->update(dt);
    }
  }
}

void Scene::draw() {
  core->getRenderer()->setRenderCamera(camera);

  for (auto component : renderableComponents) {
    component->draw(core->getRenderer());
  }

  if (Debug::isAtLevel(DebugLevel::High)) {
    // draw grid lines
    Vec2 windowSize = core->getRenderer()->getWindowSize();
    Color lineColor = Colors::White.withAlpha(0.3f);
    for (float i = 0; i < windowSize.x; i += 16) {
      core->getRenderer()->drawLine(Vec2(i, 0.0f), Vec2(i, (float)windowSize.y), lineColor);
    }
    for (float i = 0; i < windowSize.y; i += 16) {
      core->getRenderer()->drawLine(Vec2(0.0f, i), Vec2((float)windowSize.x, i), lineColor);
    }
  }
}
