#pragma once
#include "../math/math.h"
#include "../rendering/rendering.h"

using namespace std;

namespace two {

  class Entity;
  class Scene;

  struct ComponentDef {
    public:
      Uint32 componentId;
      int updateOrder;
      bool renderable;

      static bool compare(const ComponentDef& a, const ComponentDef& b);
  };

  class Component {
    private:
      Uint32 entityId;
      Scene* scene;

    protected:
      bool isEnabled;
      int sortOrder;

    public:
      Component();
      void initComponent(Uint32 _entityId, Scene* _scene);
      virtual ~Component();

      template <typename TComponent>
        static ComponentDef getComponentDef();
      static int getUpdateOrder() { return 0; }
      static bool getRenderable() { return false; }
      static bool compareByDrawOrder(const Component* a, const Component* b);

      Entity getEntity();
      Scene* getScene();
      bool getEnabled();
      void setEnabled(bool enabled);
      Transform* getTransform();

      int getSortOrder();
      void setSortOrder(int newSortOrder);

      virtual void onStart();
      virtual void onDestroy();
      virtual void update(float dt);
      virtual void draw(Renderer* renderer);
  };

  // used in classes that derive from Component
  #define SET_UPDATE_ORDER(o) static int getUpdateOrder() { return o; }
  #define SET_RENDERABLE() static bool getRenderable() { return true; }


  typedef decltype(&ComponentDef::compare) CDefCompare;
  typedef decltype(&Component::compareByDrawOrder) CDrawCompare;


  /*
   * This template magic is pulled from
   * Pikuma's 2D ECS game engine course.
   * 
   * To get the id of any component type, use
   * ComponentIDs<ComponentType>::getId();
   */
  struct cID {
    protected:
      static Uint32 nextId;
  };

  template <typename TComponent>
  class ComponentIDs : public cID {
    public:
      static Uint32 getId() {
        static Uint32 id = nextId++;
        return id;
      }
  };


  template <typename TComponent>
  ComponentDef Component::getComponentDef() {
    ComponentDef def;
    def.componentId = ComponentIDs<TComponent>::getId();
    def.updateOrder = TComponent::getUpdateOrder();
    def.renderable = TComponent::getRenderable();
    return def;
  }
}

