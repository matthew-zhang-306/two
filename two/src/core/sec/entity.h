#pragma once
#include "component.h"
#include "scene.h"
#include "../math/math.h"

using namespace std;

namespace two {
  const int MAX_COMPONENTS = 256;

  class Scene;
  class Component;

  class Entity {
    public:
      enum class state {
        // just created, but not processing updates yet.
        // this is when components should be added to the entity
        born = 0,

        // currently existing and updating.
        alive = 1,

        // killed, but not taken out of the scene yet.
        dying = 2,

        // removed. this entity will be destructed soon
        // and should be treated as garbage data.
        dead = 3
      };

    private:
      bool isEnabledSelf;
      Uint32 id;
      state entityState;
      Scene* scene;

    public:
      Entity();
      Entity(Uint32 _id, Scene* _scene);
      virtual ~Entity();

      Uint32 getId();
      state getState();
      void setState(state newState);
      Scene* getScene();
      Transform* getTransform();

      template <typename TComponent, typename ...TArgs>
        TComponent* addComponent(TArgs&& ...args);
      // can't remove a component individually. should disable it instead
      template <typename TComponent>
        bool hasComponent();
      template <typename TComponent>
        TComponent* getComponent();

      bool getEnabledSelf();
      bool getEnabledInTree();
      void setEnabled(bool enabled);
      void destroy();
  };



  template <typename TComponent, typename ...TArgs>
  TComponent* Entity::addComponent(TArgs&& ...args) {
    return scene->createComponent<TComponent>(id, forward<TArgs>(args)...);
  }

  template <typename TComponent>
  bool Entity::hasComponent() {
    return scene->hasComponent<TComponent>(id);
  }

  template <typename TComponent>
  TComponent* Entity::getComponent() {
    return scene->getComponent<TComponent>(id);
  }
}