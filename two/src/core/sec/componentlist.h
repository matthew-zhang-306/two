#pragma once
#include <unordered_map>
#include <vector>
#include "component.h"
#include "../debug/debug.h"

using namespace std;

/*
 * Implementation based on the Pool class from
 * Pikuma's 2D ECS game engine course.
 * 
 * This implementation is different in that because
 * it is storing Component* rather than fixed-sized
 * structs, it doesn't need any of the templating.
 * (Someone else can cast Component* to the right
 * subclass type later.)
 */

namespace two {

  class ComponentList {
    private:
      vector<Component*> components;
      unordered_map<Uint32, Uint32> entityIdToIndex;
      unordered_map<Uint32, Uint32> indexToEntityId;

    public:
      ComponentList();
      ~ComponentList();

      int getSize();
      void clear();

      bool set(Uint32 entityId, Component* component);
      Component* remove(Uint32 entityId);
      bool has(Uint32 entityId);
      Component* get(Uint32 entityId);

      vector<Component*>& getComponents();
  };
}
