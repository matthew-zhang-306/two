#pragma once
#include <SDL.h>
#include <string>
#include "../sec/sec.h"

using namespace std;

namespace two {
  class SpriteRenderer : public Component {
    private:
      string textureName;
      Vec2 textureSize;
      float sizeMultiplier;

      bool flipX;
      bool flipY;
      
    public:
      SpriteRenderer(string _textureName = "", Vec2 _textureSize = Vec2s::Zero, float _sizeMultiplier = 1.0f);
      ~SpriteRenderer();

      SET_RENDERABLE()

      bool getFlipX();
      bool getFlipY();
      void setFlipX(bool x);
      void setFlipY(bool y);

      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}