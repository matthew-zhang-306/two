#pragma once
#include <SDL.h>
#include <string>
#include "../sec/sec.h"

using namespace std;

namespace two {
  class BoxCollider : public Component {
    private:
      Vec2 size;

    public:
      BoxCollider(Vec2 _size = Vec2s::One);
      ~BoxCollider();

      SET_RENDERABLE()

      Rect2 getBounds();

      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}