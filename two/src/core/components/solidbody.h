#pragma once
#include "boxcollider.h"
#include "../math/math.h"
#include "../sec/sec.h"

using namespace std;

namespace two {
  class SolidBody : public Component {
    private:
      BoxCollider* boxCollider;

    public:
      SolidBody();
      ~SolidBody();

      BoxCollider* getCollider();

      void move(Vec2 amount);

      void onStart() override;
      void onDestroy() override;
      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}