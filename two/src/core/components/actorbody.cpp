#include "actorbody.h"
#include "solidbody.h"
#include "../physics/physics.h"

using namespace two;
using namespace std;

ActorBody::ActorBody() {
  boxCollider = NULL;
  collisionData = CollisionData();
}

ActorBody::~ActorBody() {

}


BoxCollider* ActorBody::getCollider() {
  return boxCollider;
}

bool ActorBody::hasPreviousCollision() {
  return collisionData.hasCollision;
}

SolidBody* ActorBody::getPreviousCollisionSolid() {
  if (collisionData.hitCollider == NULL) {
    return NULL;
  }

  return collisionData.hitCollider->getEntity().getComponent<SolidBody>();
}

Vec2 ActorBody::getPreviousCollisionNormal() {
  return collisionData.normal;
}

float ActorBody::getPreviousCollisionRemainder() {
  return collisionData.remainder;
}




void ActorBody::move(Vec2 amount) {
  collisionData = getScene()->getPhysics()->moveActor(this, amount);
  getTransform()->setPosition(collisionData.finalPosition);
}

void ActorBody::deflect(Vec2 originalAmount) {
  if (!hasPreviousCollision()) {
    return;
  }

  // TODO: implement
}

void ActorBody::slide(Vec2 originalAmount) {
  if (!hasPreviousCollision()) {
    return;
  }

  Vec2 slideAmount = (originalAmount * collisionData.remainder)
    .perpProjected(collisionData.normal);
  if (slideAmount.sqrMagnitude() > 0.0f) {
    move(slideAmount);
  }
}


void ActorBody::onStart() {
  boxCollider = getEntity().getComponent<BoxCollider>();
  getScene()->getPhysics()->addActorBody(this);
}

void ActorBody::onDestroy() {
  getScene()->getPhysics()->removeActorBody(this);
}

void ActorBody::update(float dt) {

}

void ActorBody::draw(Renderer* renderer) {

}
