#include "slopecollider.h"

using namespace std;
using namespace two;

SlopeCollider::SlopeCollider(Vec2 _size, Vec2 _orientation) {
  size = _size;
  orientation = _orientation;
  if (Mathf::signum(orientation.x) == 0.0f || Mathf::signum(orientation.y) == 0.0f) {
    Debug::error("SlopeCollider::SlopeCollider", "invalid slope orientation " + orientation);
  }

  sortOrder = 100;
}

SlopeCollider::~SlopeCollider() {

}


Rect2 SlopeCollider::getBounds() {
  return Rect2::fromCenter(
    getTransform()->getPosition(),
    getTransform()->getScale() * size
  );
}

Vec2 SlopeCollider::getNormal() {
  Rect2 bounds = getBounds();
  return Vec2(bounds.h * Mathf::sign(orientation.x), bounds.w * Mathf::sign(orientation.y)).normalized();
}


void SlopeCollider::update(float dt) {

}

void SlopeCollider::draw(Renderer* renderer) {
  if (!Debug::isAtLevel(DebugLevel::High)) {
    return;
  }

  Rect2 bounds = getBounds();
  
  if (orientation.x > 0.0f && orientation.y > 0.0f) {
    // facing down right
    renderer->drawLine(bounds.topLeft(), bounds.topRight(), Colors::Red);
    renderer->drawLine(bounds.topLeft(), bounds.bottomLeft(), Colors::Red);
    renderer->drawLine(bounds.bottomLeft(), bounds.topRight(), Colors::Red);
  }
  else if (orientation.x > 0.0f && orientation.y < 0.0f) {
    // facing up right
    renderer->drawLine(bounds.topLeft(), bounds.bottomLeft(), Colors::Red);
    renderer->drawLine(bounds.bottomLeft(), bounds.bottomRight(), Colors::Red);
    renderer->drawLine(bounds.topLeft(), bounds.bottomRight(), Colors::Red);
  }
  else if (orientation.x < 0.0f && orientation.y > 0.0f) {
    // facing down left
    renderer->drawLine(bounds.topLeft(), bounds.topRight(), Colors::Red);
    renderer->drawLine(bounds.topRight(), bounds.bottomRight(), Colors::Red);
    renderer->drawLine(bounds.topLeft(), bounds.bottomRight(), Colors::Red);
  }
  else if (orientation.x < 0.0f && orientation.y < 0.0f) {
    // facing up left
    renderer->drawLine(bounds.bottomRight(), bounds.topRight(), Colors::Red);
    renderer->drawLine(bounds.bottomRight(), bounds.bottomLeft(), Colors::Red);
    renderer->drawLine(bounds.bottomLeft(), bounds.topRight(), Colors::Red);
  }
}