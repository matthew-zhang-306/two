#pragma once
#include "boxcollider.h"
#include "../math/math.h"
#include "../sec/sec.h"
#include "../physics/collisiondata.h"

using namespace std;

namespace two {
  
  class SolidBody;

  class ActorBody : public Component {
    private:
      BoxCollider* boxCollider;
      CollisionData collisionData;

    public:
      ActorBody();
      ~ActorBody();

      BoxCollider* getCollider();
      bool hasPreviousCollision();
      SolidBody* getPreviousCollisionSolid();
      Vec2 getPreviousCollisionNormal();
      float getPreviousCollisionRemainder();
      
      void move(Vec2 amount);
      void deflect(Vec2 originalAmount);
      void slide(Vec2 originalAmount);

      void onStart() override;
      void onDestroy() override;
      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}