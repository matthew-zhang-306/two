#include "boxcollider.h"

using namespace std;
using namespace two;

BoxCollider::BoxCollider(Vec2 _size) {
  size = _size;
  sortOrder = 100;
}

BoxCollider::~BoxCollider() {

}


Rect2 BoxCollider::getBounds() {
  return Rect2::fromCenter(
    getTransform()->getPosition(),
    getTransform()->getScale() * size
  );
}


void BoxCollider::update(float dt) {

}

void BoxCollider::draw(Renderer* renderer) {
  if (!Debug::isAtLevel(DebugLevel::High)) {
    return;
  }

  renderer->drawWireRect(getBounds(), Colors::Red);
}