#include "solidbody.h"
#include "actorbody.h"
#include "../physics/physics.h"

using namespace two;
using namespace std;

SolidBody::SolidBody() {
  boxCollider = NULL;
}

SolidBody::~SolidBody() {

}


BoxCollider* SolidBody::getCollider() {
  return boxCollider;
}


void SolidBody::move(Vec2 amount) {
  getScene()->getPhysics()->moveSolid(this, amount);
}


void SolidBody::onStart() {
  boxCollider = getEntity().getComponent<BoxCollider>();
  getScene()->getPhysics()->addSolidBody(this);
}

void SolidBody::onDestroy() {
  getScene()->getPhysics()->removeSolidBody(this);
}

void SolidBody::update(float dt) {

}

void SolidBody::draw(Renderer* renderer) {

}
