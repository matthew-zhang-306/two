#pragma once
#include <unordered_set>
#include <SDL.h>
#include "boxcollider.h"
#include "../math/math.h"
#include "../sec/sec.h"

using namespace std;

namespace two {

  class SolidBody;
  class ActorBody;

  class TriggerBody : public Component {
    private:
      BoxCollider* boxCollider;
      unordered_set<BoxCollider*> collidingBodies;

    public:
      TriggerBody();
      ~TriggerBody();

      BoxCollider* getCollider();
      unordered_set<BoxCollider*> getCollidingBodies();
      Uint32 getNumCollidingBodies();

      void move(Vec2 amount);
      void onEnterTrigger(BoxCollider* boxCollider);
      void onExitTrigger(BoxCollider* boxCollider);

      void onStart() override;
      void onDestroy() override;
      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}