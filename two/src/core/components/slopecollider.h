#pragma once
#include <SDL.h>
#include <string>
#include "../sec/sec.h"

using namespace std;

namespace two {
  class SlopeCollider : public Component {
    private:
      Vec2 size;

      // sign(x) and sign(y) should match with the slope's normal vector
      Vec2 orientation;

    public:
      SlopeCollider(Vec2 _size = Vec2s::One, Vec2 _orientation = Vec2(-1.0f, -1.0));
      ~SlopeCollider();

      SET_RENDERABLE()

      Rect2 getBounds();
      Vec2 getNormal();

      void update(float dt) override;
      void draw(Renderer* renderer) override;
  };
}