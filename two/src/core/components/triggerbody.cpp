#include "triggerbody.h"
#include "actorbody.h"
#include "solidbody.h"
#include "../physics/physics.h"

using namespace two;
using namespace std;

TriggerBody::TriggerBody() {
  boxCollider = NULL;
}

TriggerBody::~TriggerBody() {

}


BoxCollider* TriggerBody::getCollider() {
  return boxCollider;
}

unordered_set<BoxCollider*> TriggerBody::getCollidingBodies() {
  return collidingBodies;
}

Uint32 TriggerBody::getNumCollidingBodies() {
  return collidingBodies.size();
}


void TriggerBody::move(Vec2 amount) {
  getScene()->getPhysics()->moveTrigger(this, amount);
}

void TriggerBody::onEnterTrigger(BoxCollider* boxCollider) {
  collidingBodies.insert(boxCollider);
  // TODO: some delegate call here
}

void TriggerBody::onExitTrigger(BoxCollider* boxCollider) {
  collidingBodies.erase(boxCollider);
  // TODO: some delegate call here
}


void TriggerBody::onStart() {
  boxCollider = getEntity().getComponent<BoxCollider>();
  getScene()->getPhysics()->addTriggerBody(this);
}

void TriggerBody::onDestroy() {
  getScene()->getPhysics()->removeTriggerBody(this);
  collidingBodies.clear();
}

void TriggerBody::update(float dt) {

}

void TriggerBody::draw(Renderer* renderer) {

}
