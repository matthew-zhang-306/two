#pragma once
#include "spriterenderer.h"
#include "../core.h"
#include "../assets/assetmanager.h"
#include <iostream>

using namespace std;
using namespace two;

SpriteRenderer::SpriteRenderer(string _textureName, Vec2 _textureSize, float _sizeMultiplier) {
  textureName = _textureName;
  textureSize = _textureSize;
  sizeMultiplier = _sizeMultiplier;
}

SpriteRenderer::~SpriteRenderer() {

}


bool SpriteRenderer::getFlipX() {
  return flipX;
}

bool SpriteRenderer::getFlipY() {
  return flipY;
}

void SpriteRenderer::setFlipX(bool x) {
  flipX = x;
}

void SpriteRenderer::setFlipY(bool y) {
  flipY = y;
}


void SpriteRenderer::update(float dt) {

}

void SpriteRenderer::draw(Renderer* renderer) {
  AssetManager* assetManager = getScene()->getCore()->getAssetManager();
  SDL_Texture* texture = assetManager->getTexture(textureName);
  Rect2 srcRect = Rect2(Vec2s::Zero, textureSize);
  Rect2 destRect = Rect2::fromCenter(
    getTransform()->getPosition(),
    textureSize * sizeMultiplier * getTransform()->getScale()
  );
  
  renderer->drawTexture(texture, srcRect, destRect,
    getTransform()->getRotation(), flipX, flipY);
}