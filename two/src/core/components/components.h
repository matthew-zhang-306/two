#pragma once

#include "actorbody.h"
#include "boxcollider.h"
#include "slopecollider.h"
#include "solidbody.h"
#include "spriterenderer.h"
#include "triggerbody.h"