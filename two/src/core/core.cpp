#include "core.h"
#include "components/components.h"
#include "math/math.h"
#include <iostream>
#include <string>

using namespace std;
using namespace two;

Core::Core() {
  isRunning = false;
  oldTicks = 0;
  assetManager = new AssetManager();
  input = new Input();
  renderer = new Renderer();
  currentScene = NULL;
  nextScene = NULL;

  // set up debugging
  Debug::core = this;
  Debug::level = DebugLevel::Low;
  Debug::pauseOnError = false;
}
Core::~Core() {
  delete assetManager;
  delete input;
  delete renderer;
}


AssetManager* Core::getAssetManager() {
  return assetManager;
}
Input* Core::getInput() {
  return input;
}
Renderer* Core::getRenderer() {
  return renderer;
}
Scene* Core::getCurrentScene() {
  return currentScene;
}


void Core::init() {
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    Debug::error("Core::init", "couldn't initialize SDL");
    return;
  }
  
  // TODO: make the window scale a member variable
  renderer->setWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT, 4.0f);
  if (!renderer->init()) {
    return;
  }

  isRunning = true;
  Debug::log("Core", "game initialized!");
}

void Core::run() {
  if (!isRunning) {
    return;
  }

  setup();
  while (isRunning) {
    input->processInput(this);
    
    // check if the game is still running at each step
    // if the game has quit, avoid unnecessary work
    if (isRunning) {
      doECLifeCycle();
    }
    if (isRunning) {
      float dt = waitForFrame();
      update(dt);
    }
    if (isRunning) {
      render();
    }

    if (isRunning && nextScene != NULL) {
      switchScenes();
    }
  }
  exit();
}

void Core::destroy() {
  if (renderer != NULL) {
    renderer->destroy();
  }

  SDL_Quit();
}


void Core::setup() {
  if (nextScene == NULL) {
    // no scene loaded. just load an empty one
    currentScene = new Scene();
    currentScene->load(this);
  }
  else {
    switchScenes();
  }
}

float Core::waitForFrame() {
  // wait for the next frame
  uint32_t ticksPerFrame = (uint32_t)floorf(1000.0f / TARGET_FPS);
  uint32_t previousFrameLength = SDL_GetTicks() - oldTicks;
  if (previousFrameLength < ticksPerFrame) {
    // the previous frame ended early, so wait until the next frame should start
    SDL_Delay(ticksPerFrame - previousFrameLength);
  }

  // determine how long ago the previous frame was (dt).
  // dt will vary but in case of extreme lag will be capped to the minimum fps
  uint32_t ticks = SDL_GetTicks();
  float dt = (ticks - oldTicks) / 1000.0f;
  if (dt > 1.0f / MINIMUM_FPS) {
    dt = 1.0f / MINIMUM_FPS;
    Debug::warn("--- FRAME ---", "running below minimum fps", DebugLevel::High);
  }

  Debug::log("--- FRAME ---",
    "dt = " + Debug::f(dt) + " (previous frame lasted " + Debug::u(ticks - oldTicks) + " ms)",
    DebugLevel::High
  );
  oldTicks = ticks;

  return dt;
}

void Core::doECLifeCycle() {
  currentScene->doECLifeCycle();
}

void Core::update(float dt) {
  currentScene->update(dt);
}

void Core::render() {
  renderer->clear();
  currentScene->draw();
  renderer->present();
}

void Core::switchScenes() {
  if (currentScene != NULL) {
    currentScene->unload();
    delete currentScene;
  }

  currentScene = nextScene;
  nextScene = NULL;

  if (currentScene != NULL) {
    currentScene->load(this);
  }
}

void Core::exit() {
  // TODO: unload everything
  nextScene = NULL;
  switchScenes();
}


void Core::toggleDebug() {
  if (Debug::level != DebugLevel::High) {
    Debug::level = DebugLevel::High;
  }
  else {
    Debug::level = DebugLevel::Low;
  }
}

void Core::suspend() {
  // don't use Debug for this statement
  cout << "game suspended. type nothing to continue, type anything to quit" << endl;

  string s;
  cin >> s;

  if (s.size() > 0) {
    quitGame();
  }
}

void Core::quitGame() {
  isRunning = false;
}