#include "input.h"
#include "core.h"
#include <iostream>

using namespace std;
using namespace two;

Input::Input() {
  // yes
  sdlkToKeyCode.emplace(SDLK_LEFT     , KeyCode::Left     );
  sdlkToKeyCode.emplace(SDLK_RIGHT    , KeyCode::Right    );
  sdlkToKeyCode.emplace(SDLK_UP       , KeyCode::Up       );
  sdlkToKeyCode.emplace(SDLK_DOWN     , KeyCode::Down     );
  sdlkToKeyCode.emplace(SDLK_a        , KeyCode::A        );
  sdlkToKeyCode.emplace(SDLK_b        , KeyCode::B        );
  sdlkToKeyCode.emplace(SDLK_c        , KeyCode::C        );
  sdlkToKeyCode.emplace(SDLK_d        , KeyCode::D        );
  sdlkToKeyCode.emplace(SDLK_e        , KeyCode::E        );
  sdlkToKeyCode.emplace(SDLK_f        , KeyCode::F        );
  sdlkToKeyCode.emplace(SDLK_g        , KeyCode::G        );
  sdlkToKeyCode.emplace(SDLK_h        , KeyCode::H        );
  sdlkToKeyCode.emplace(SDLK_i        , KeyCode::I        );
  sdlkToKeyCode.emplace(SDLK_j        , KeyCode::J        );
  sdlkToKeyCode.emplace(SDLK_k        , KeyCode::K        );
  sdlkToKeyCode.emplace(SDLK_l        , KeyCode::L        );
  sdlkToKeyCode.emplace(SDLK_m        , KeyCode::M        );
  sdlkToKeyCode.emplace(SDLK_n        , KeyCode::N        );
  sdlkToKeyCode.emplace(SDLK_o        , KeyCode::O        );
  sdlkToKeyCode.emplace(SDLK_p        , KeyCode::P        );
  sdlkToKeyCode.emplace(SDLK_q        , KeyCode::Q        );
  sdlkToKeyCode.emplace(SDLK_r        , KeyCode::R        );
  sdlkToKeyCode.emplace(SDLK_s        , KeyCode::S        );
  sdlkToKeyCode.emplace(SDLK_t        , KeyCode::T        );
  sdlkToKeyCode.emplace(SDLK_u        , KeyCode::U        );
  sdlkToKeyCode.emplace(SDLK_v        , KeyCode::V        );
  sdlkToKeyCode.emplace(SDLK_w        , KeyCode::W        );
  sdlkToKeyCode.emplace(SDLK_x        , KeyCode::X        );
  sdlkToKeyCode.emplace(SDLK_y        , KeyCode::Y        );
  sdlkToKeyCode.emplace(SDLK_z        , KeyCode::Z        );
  sdlkToKeyCode.emplace(SDLK_0        , KeyCode::D0       );
  sdlkToKeyCode.emplace(SDLK_1        , KeyCode::D1       );
  sdlkToKeyCode.emplace(SDLK_2        , KeyCode::D2       );
  sdlkToKeyCode.emplace(SDLK_3        , KeyCode::D3       );
  sdlkToKeyCode.emplace(SDLK_4        , KeyCode::D4       );
  sdlkToKeyCode.emplace(SDLK_5        , KeyCode::D5       );
  sdlkToKeyCode.emplace(SDLK_6        , KeyCode::D6       );
  sdlkToKeyCode.emplace(SDLK_7        , KeyCode::D7       );
  sdlkToKeyCode.emplace(SDLK_8        , KeyCode::D8       );
  sdlkToKeyCode.emplace(SDLK_9        , KeyCode::D9       );
  sdlkToKeyCode.emplace(SDLK_SPACE    , KeyCode::Space    );
  sdlkToKeyCode.emplace(SDLK_LCTRL    , KeyCode::LCtrl    );
  sdlkToKeyCode.emplace(SDLK_LSHIFT   , KeyCode::LShift   );
  sdlkToKeyCode.emplace(SDLK_LALT     , KeyCode::LAlt     );
  sdlkToKeyCode.emplace(SDLK_RCTRL    , KeyCode::RCtrl    );
  sdlkToKeyCode.emplace(SDLK_RSHIFT   , KeyCode::RShift   );
  sdlkToKeyCode.emplace(SDLK_RALT     , KeyCode::RAlt     );
  sdlkToKeyCode.emplace(SDLK_RETURN   , KeyCode::Enter    );
  sdlkToKeyCode.emplace(SDLK_BACKSPACE, KeyCode::Backspace);
  sdlkToKeyCode.emplace(SDLK_ESCAPE   , KeyCode::Esc      );
  sdlkToKeyCode.emplace(SDLK_SLASH    , KeyCode::Slash    );
  sdlkToKeyCode.emplace(SDLK_BACKSLASH, KeyCode::Backslash);
  sdlkToKeyCode.emplace(SDLK_UNKNOWN  , KeyCode::Unknown  );
}
Input::~Input() {

}


void Input::processInput(Core* core) {
  previousInput = currentInput;

  SDL_Event sdlEvent;
  SDL_Keycode sdlKey;

  while (SDL_PollEvent(&sdlEvent)) {
    switch (sdlEvent.type) {
    case SDL_QUIT:
      // window closed.
      core->quitGame();
      break;
    case SDL_KEYDOWN:
      sdlKey = sdlEvent.key.keysym.sym;
      if (sdlkToKeyCode.find(sdlKey) != sdlkToKeyCode.end()) {
        KeyCode key = sdlkToKeyCode[sdlKey];
        currentInput.set(static_cast<size_t>(key), true);
      }
      break;
    case SDL_KEYUP:
      sdlKey = sdlEvent.key.keysym.sym;
      if (sdlkToKeyCode.find(sdlKey) != sdlkToKeyCode.end()) {
        KeyCode key = sdlkToKeyCode[sdlKey];
        currentInput.set(static_cast<size_t>(key), false);
      }
      break;
    default:
      break;
    }
  }

  // check for debug
  if (getKeyPressed(KeyCode::Backslash)) {
    core->toggleDebug();
  }
}

bool Input::getKeyHeld(KeyCode keyCode) {
  return currentInput.test(static_cast<size_t>(keyCode));
}

bool Input::getKeyWasHeld(KeyCode keyCode) {
  return previousInput.test(static_cast<size_t>(keyCode));
}

bool Input::getKeyPressed(KeyCode keyCode) {
  return currentInput.test(static_cast<size_t>(keyCode)) &&
    !previousInput.test(static_cast<size_t>(keyCode));
}

bool Input::getKeyReleased(KeyCode keyCode) {
  return !currentInput.test(static_cast<size_t>(keyCode)) &&
    previousInput.test(static_cast<size_t>(keyCode));
}
