#include "renderer.h"
#include "../debug/debug.h"
#include <iostream>

using namespace std;
using namespace two;

Renderer::Renderer() {
  sdlWindow = NULL;
  sdlRenderer = NULL;

  // set reasoanble defaults
  setWindowSize(800, 450, 1.0f);
  renderCamera = Camera(Rect2(Vec2s::Zero, getWindowSize()));
}

Renderer::~Renderer() {

}


bool Renderer::init() {
  SDL_DisplayMode displayMode;
  SDL_GetCurrentDisplayMode(0, &displayMode);

  sdlWindow = SDL_CreateWindow(
    NULL,
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    static_cast<int>(windowWidth * windowScale),
    static_cast<int>(windowHeight * windowScale),
    SDL_WINDOW_SHOWN
  );
  if (!sdlWindow) {
    Debug::error("Renderer::init", "couldn't open SDL window");
    return false;
  }

  sdlRenderer = SDL_CreateRenderer(
    sdlWindow,
    -1,
    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (!sdlRenderer) {
    Debug::error("Renderer::init", "couldn't create SDL window");
    return false;
  }

  // SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
  SDL_RenderSetScale(sdlRenderer, windowScale, windowScale);
  SDL_SetRenderDrawBlendMode(sdlRenderer, SDL_BLENDMODE_BLEND);

  return true;
}

void Renderer::destroy() {
  SDL_DestroyRenderer(sdlRenderer);
  SDL_DestroyWindow(sdlWindow);
}


SDL_Window* Renderer::getSDLWindow() {
  return sdlWindow;
}

SDL_Renderer* Renderer::getSDLRenderer() {
  return sdlRenderer;
}


void Renderer::clear() {
  // TODO: replcae with a "clearColor" member variable, when you end up making a Color struct
  SDL_SetRenderDrawColor(sdlRenderer, 0x64, 0x95, 0xed, 0xff);
  SDL_RenderClear(sdlRenderer);
}

void Renderer::present() {
  SDL_RenderPresent(sdlRenderer);
}


Vec2 Renderer::getWindowSize() {
  return Vec2((float)windowWidth, (float)windowHeight);
}

void Renderer::setWindowSize(int width, int height, float scale) {
  windowWidth = width;
  windowHeight = height;
  windowScale = scale;
}


void Renderer::setRenderCamera(Camera camera) {
  renderCamera = camera;
}


void Renderer::setColor(Color color) {
  SDL_SetRenderDrawColor(sdlRenderer,
    (int)floor(color.r * 255.0f),
    (int)floor(color.g * 255.0f), 
    (int)floor(color.b * 255.0f),
    (int)floor(color.a * 255.0f)
  );
}

void Renderer::drawLine(Vec2 start, Vec2 end, Color color) {
  setColor(color);

  Vec2 s = renderCamera.worldToViewport(start) * getWindowSize();
  Vec2 e = renderCamera.worldToViewport(end) * getWindowSize();

  SDL_RenderDrawLine(sdlRenderer,
    (int)round(s.x),
    (int)round(s.y),
    (int)round(e.x),
    (int)round(e.y)
  );
}

void Renderer::drawRect(Rect2 rect, Color color) {
  Rect2 r = Rect2::fromMinMax(
    renderCamera.worldToViewport(rect.min()) * getWindowSize(),
    renderCamera.worldToViewport(rect.max()) * getWindowSize()
  );

  SDL_Rect sdlRect = r.toSDL();
  setColor(color);
  SDL_RenderFillRect(sdlRenderer, &sdlRect);
}

void Renderer::drawWireRect(Rect2 rect, Color color) {
  Rect2 r = Rect2::fromMinMax(
    renderCamera.worldToViewport(rect.min()) * getWindowSize(),
    renderCamera.worldToViewport(rect.max()) * getWindowSize()
  );

  SDL_Rect sdlRect = r.toSDL();
  setColor(color);
  SDL_RenderDrawRect(sdlRenderer, &sdlRect);
}

void Renderer::drawTexture(SDL_Texture* texture, Rect2 srcRect, Rect2 destRect,
                            float angle, bool flipX, bool flipY) {
  Rect2 d = Rect2::fromMinMax(
    renderCamera.worldToViewport(destRect.min()) * getWindowSize(),
    renderCamera.worldToViewport(destRect.max()) * getWindowSize()
  );

  SDL_Rect sdlSrcRect = srcRect.toSDL();
  SDL_Rect sdlDestRect = d.toSDL();
  SDL_RendererFlip sdlFlip = static_cast<SDL_RendererFlip>(
    (flipX ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE) |
    (flipY ? SDL_FLIP_VERTICAL : SDL_FLIP_NONE)
  );
  SDL_RenderCopyEx(sdlRenderer, texture, &sdlSrcRect, &sdlDestRect, 
                    angle, NULL, sdlFlip);
}
