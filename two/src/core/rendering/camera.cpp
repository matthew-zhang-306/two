#include "camera.h"

using namespace std;
using namespace two;

Camera::Camera() {
  // use a sensible default
  viewportRect = Rect2(0.0f, 0.0f, 800.0f, 450.0f);
  aspectRatio = viewportRect.w / viewportRect.h;
}

Camera::Camera(Rect2 screenRect) {
  viewportRect = screenRect;
  aspectRatio = viewportRect.w / viewportRect.h;
}

Camera::~Camera() {

}


Rect2 Camera::getViewportRect() {
  return viewportRect;
}

Vec2 Camera::getPosition() {
  return viewportRect.center();
}

float Camera::getWidth() {
  return viewportRect.w;
}

void Camera::setPosition(Vec2 position) {
  viewportRect = viewportRect.withPos(position, Vec2s::Center);
}

void Camera::setWidth(float width) {
  viewportRect = viewportRect.withSize(Vec2(width, width / aspectRatio), Vec2s::Center);
}


Vec2 Camera::worldToViewport(Vec2 world) {
  return (world - viewportRect.pos()) / viewportRect.size();
}

Vec2 Camera::viewportToWorld(Vec2 viewport) {
  return viewport * viewportRect.size() + viewportRect.pos();
}
