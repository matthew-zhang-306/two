#pragma once
#include <SDL.h>
#include "camera.h"
#include "../math/math.h"

using namespace std;

namespace two {
  class Renderer {
    private:
      SDL_Window* sdlWindow;
      SDL_Renderer* sdlRenderer;
      Camera renderCamera;
      
      int windowWidth;
      int windowHeight;
      float windowScale;

      void setColor(Color color);

    public:
      Renderer();
      ~Renderer();

      SDL_Window* getSDLWindow();
      SDL_Renderer* getSDLRenderer();

      bool init();
      void destroy();
      void clear();
      void present();

      Vec2 getWindowSize();
      void setWindowSize(int width, int height, float scale);

      void setRenderCamera(Camera camera);

      void drawLine(Vec2 start, Vec2 end, Color color);
      void drawRect(Rect2 rect, Color color);
      void drawWireRect(Rect2 rect, Color color);
      void drawTexture(SDL_Texture* texture, Rect2 srcRect, Rect2 destRect,
                        float angle = 0.0f, bool flipX = false, bool flipY = false);
  };
}