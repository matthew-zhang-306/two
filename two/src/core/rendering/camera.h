#pragma once
#include "../math/math.h"

using namespace std;

namespace two {
  class Camera {
    private:
      Rect2 viewportRect;
      float aspectRatio; // = width / height

    public:
      Camera();
      Camera(Rect2 screenRect);
      ~Camera();

      Rect2 getViewportRect();
      Vec2 getPosition();
      float getWidth();
      void setPosition(Vec2 position);
      void setWidth(float width);

      Vec2 worldToViewport(Vec2 world);
      Vec2 viewportToWorld(Vec2 viewport);
  };
}