#pragma once
#include <unordered_set>
#include "collisiondata.h"
#include "../math/math.h"
#include "../components/components.h"

using namespace std;

namespace two {
  class Physics {
    private:
      unordered_set<SolidBody*> solidBodies;
      unordered_set<ActorBody*> actorBodies;
      unordered_set<TriggerBody*> triggerBodies;

    public:
      Physics();
      ~Physics();

      void addSolidBody(SolidBody* solid);
      void removeSolidBody(SolidBody* solid);
      void addActorBody(ActorBody* actor);
      void removeActorBody(ActorBody* actor);
      void addTriggerBody(TriggerBody* trigger);
      void removeTriggerBody(TriggerBody* trigger);
      
      void moveSolid(SolidBody* solid, Vec2 amount);
      CollisionData moveActor(ActorBody* actor, Vec2 amount);
      void moveTrigger(TriggerBody* trigger, Vec2 amount);

      // performs a ray/AABB intersection check.
      // the ray is defined as starting at rayOrigin and ending at rayOrigin + rayDir
      // returns the fraction of ||rayDir|| that the intersection point is at
      // writes the intersection point into intersection
      // writes the normal of the intersection into normal unless there was no intersection or the ray started inside the rectangle
      float rayAABB(Rect2 rect, Vec2 rayOrigin, Vec2 rayDir, Vec2* intersection, Vec2* normal);

      // performs the swept AABB collision check.
      // returns the fraction of velocityA that rectA can move to not hit rectB
      // and, if rectA hit rectB, writes the normal of the collision into normal
      // will discard any collision results that happen after the given maxFraction
      // if rectA is inside rectB, it lets rectA move out of or parallel to rectB, but not into it
      float sweptAABB(Rect2 rectA, Rect2 rectB, Vec2 velocityA, Vec2* normal, float maxFraction = 1.0f);

      // performs a collision check between a moving rectangle and a static slope.
      // returns the fraction of velocity that the rect can move without hitting the slope
      // and, if there was a hit, writes the normal of the collision into normal
      float AABBslope(Rect2 rect, Rect2 slopeRect, Vec2 slopeOrientation, Vec2 velocity, Vec2* normal);

      // returns a vector that pushes the point out of the rectangle in the correct direction
      // returns 0,0 if the point is outside the rectangle
      Vec2 getPushVector(Vec2 point, Rect2 rect);

      // returns a vector that pushes rectA out of rectB in the correct direction
      // returns 0,0 if the rects are not intersecting
      Vec2 getPushVector(Rect2 rectA, Rect2 rectB);
  };
}