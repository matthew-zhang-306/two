#pragma once
#include "../math/math.h"

using namespace std;

namespace two {
  class BoxCollider;

  struct CollisionData {
    public:
      bool hasCollision;
      BoxCollider* movingCollider;
      BoxCollider* hitCollider;
      Vec2 startPosition;
      Vec2 finalPosition;
      Vec2 velocity;
      float time;
      float remainder;
      Vec2 normal;

      CollisionData() {
        hasCollision = false;
        movingCollider = NULL;
        hitCollider = NULL;
        startPosition = Vec2s::Zero;
        finalPosition = Vec2s::Zero;
        velocity = Vec2s::Zero;
        time = 1.0f;
        remainder = 0.0f;
        normal = Vec2s::Zero;
      }
      ~CollisionData() {}
  };
}