#include "physics.h"
#include "../debug/debug.h"

using namespace two;
using namespace std;

Physics::Physics() {

}

Physics::~Physics() {

}


void Physics::addSolidBody(SolidBody* solid) {
  if (solidBodies.find(solid) != solidBodies.end()) {
    Debug::error("Physics::addSolidBody", "trying to add a solid body that already exists!");
    return;
  }

  solidBodies.insert(solid);
}

void Physics::removeSolidBody(SolidBody* solid) {
  if (solidBodies.find(solid) == solidBodies.end()) {
    Debug::error("Physics::removeSolidBody", "trying to remove a solid body that doesn't exist!");
    return;
  }

  solidBodies.erase(solid);
}

void Physics::addActorBody(ActorBody* actor) {
  if (actorBodies.find(actor) != actorBodies.end()) {
    Debug::error("Physics::addActorBody", "trying to add an actor body that already exists!");
    return;
  }

  actorBodies.insert(actor);
}

void Physics::removeActorBody(ActorBody* actor) {
  if (actorBodies.find(actor) == actorBodies.end()) {
    Debug::error("Physics::removeActorBody", "trying to remove an actor body that doesn't exist!");
    return;
  }

  actorBodies.erase(actor);
}

void Physics::addTriggerBody(TriggerBody* trigger) {
  if (triggerBodies.find(trigger) != triggerBodies.end()) {
    Debug::error("Physics::addTriggerBody", "trying to add a trigger body that already exists!");
    return;
  }

  triggerBodies.insert(trigger);
}

void Physics::removeTriggerBody(TriggerBody* trigger) {
  if (triggerBodies.find(trigger) == triggerBodies.end()) {
    Debug::error("Physics::removeTriggerBody", "trying to remove a trigger body that doesn't exist!");
    return;
  }

  triggerBodies.erase(trigger);
}


void Physics::moveSolid(SolidBody* solid, Vec2 amount) {
  // TODO
}

/**
 * This function implements swept AABB collision, described here:
 * https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/swept-aabb-collision-detection-and-response-r3084/
 */
CollisionData Physics::moveActor(ActorBody* actor, Vec2 amount) {
  Rect2 actorBounds = actor->getCollider()->getBounds();

  CollisionData data = CollisionData();
  data.movingCollider = actor->getCollider();
  data.velocity = amount;
  data.time = 1.0f;
  data.remainder = 0.0f;
  data.startPosition = actor->getTransform()->getPosition();
  data.finalPosition = data.startPosition + data.velocity * data.time;

  for (SolidBody* solid : solidBodies) {
    Rect2 solidBounds = solid->getCollider()->getBounds();

    if (solidBounds.intersects(actorBounds)) {
      Vec2 pushVec = getPushVector(actorBounds, solidBounds);
      Debug::warn("Physics::moveActor", "found an actor inside a solid! this will likely happen due to floating point rounding errors but if the overlap is sizeable then this shouldn't happen. actor: " + actorBounds + ", solid: " + solidBounds + ", overlap: " + actorBounds.overlap(solidBounds) + ", pushVec: " + pushVec, DebugLevel::High);

      if (pushVec.dot(amount) <= 0) {
        // the actor isn't moving away from the solid, so we'll return a collision that will hopefully push it out
        data.hasCollision = true;
        data.hitCollider = solid->getCollider();
        data.time = 0.0f;
        data.remainder = 1.0f;
        data.finalPosition = data.startPosition + pushVec;
        data.normal = pushVec.normalized();
        break;
      }
    }

    Vec2 collisionNormal;
    float collisionTime = sweptAABB(actorBounds, solidBounds, amount, &collisionNormal, data.time);

    if (collisionTime < data.time) {
      // this is a collision
      data.hasCollision = true;
      data.hitCollider = solid->getCollider();
      data.time = collisionTime;
      data.remainder = 1 - collisionTime;
      data.finalPosition = data.startPosition + data.velocity * data.time;
      data.normal = collisionNormal;

      if (collisionTime <= 0.0001f) {
        // its probably not worth checking for more collisions
        break;
      }
    }
  }

  return data;
}

void Physics::moveTrigger(TriggerBody* trigger, Vec2 amount) {
  // TODO
}


/*
* Implementation for ray AABB intersection described here:
* https://noonat.github.io/intersect/
*/
float Physics::rayAABB(Rect2 rect, Vec2 rayOrigin, Vec2 rayDir, Vec2* intersection, Vec2* normal) {
  if (rect.contains(rayOrigin)) {
    // ray started inside the rectangle
    if (intersection != NULL) {
      *intersection = rayOrigin;
    }
    return 0.0f;
  }

  // check if the ray has any length. if it doesn't then we know there's no intersection
  if (rayDir != Vec2s::Zero) {
    Vec2 invRayDir = Vec2(1.0f / rayDir.x, 1.0f / rayDir.y);
    Vec2 enter = Vec2s::NegInf;
    Vec2 exit = Vec2s::Inf;

    // get enter/exit times in both directions by solving linear equations
    if (rayDir.x > 0.0f) {
      enter.x = (rect.left() - rayOrigin.x) * invRayDir.x;
      exit.x = (rect.right() - rayOrigin.x) * invRayDir.x;
    }
    if (rayDir.x < 0.0f) {
      enter.x = (rect.right() - rayOrigin.x) * invRayDir.x;
      exit.x = (rect.left() - rayOrigin.x) * invRayDir.x;
    }
    if (rayDir.y > 0.0f) {
      enter.y = (rect.top() - rayOrigin.y) * invRayDir.y;
      exit.y = (rect.bottom() - rayOrigin.y) * invRayDir.y;
    }
    if (rayDir.y < 0.0f) {
      enter.y = (rect.bottom() - rayOrigin.y) * invRayDir.y;
      exit.y = (rect.top() - rayOrigin.y) * invRayDir.y;
    }

    float maxEntry = max(enter.x, enter.y);
    float minExit = min(exit.x, exit.y);

    // enter/exit intervals have to overlap between 0 and 1
    if (maxEntry < minExit && maxEntry >= 0.0f && maxEntry < 1.0f) {
      // this is an intersection
      if (intersection != NULL) {
        *intersection = rayOrigin + rayDir * maxEntry;
      }
      if (normal != NULL) {
        if (enter.x > enter.y) {
          *normal = Vec2(-Mathf::sign(rayDir.x), 0.0f);
        }
        else {
          *normal = Vec2(0.0f, -Mathf::sign(rayDir.y));
        }
      }
      return maxEntry;
    }
  }

  // intersection check failed
  if (intersection != NULL) {
    *intersection = rayOrigin + rayDir;
  }
  return 1.0f;
}

/**
* Implementation for swept AABB collision described here:
* https://noonat.github.io/intersect/
*/
float Physics::sweptAABB(Rect2 rectA, Rect2 rectB, Vec2 velocityA, Vec2* normal, float maxFraction) {
  if (rectA.intersects(rectB)) {
    // rectangles are intersecting. for now we handle this by allowing rectA to clip through rectB, so long as its moving away from it
    Vec2 pushVector = getPushVector(rectA, rectB);
    if (pushVector.dot(velocityA) >= 0.0f) {
      return maxFraction;
    }
    else {
      if (normal != NULL) {
        *normal = pushVector.normalized();
        return 0.0f;
      }
    }
  }
  if (!rectB.intersects(rectA.combine(rectA.translated(velocityA * maxFraction)))) {
    // rectB is nowhere near rectA
    return maxFraction;
  }

  // do a ray check with an inflated b
  Vec2 intersectNormal = Vec2s::Zero;
  float time = rayAABB(rectB.inflate(rectA.size()), rectA.center(), velocityA, NULL, &intersectNormal);
  if (time < maxFraction && normal != NULL) {
    *normal = intersectNormal;
    return time;
  }
  else {
    return maxFraction;
  }
}

float Physics::AABBslope(Rect2 rect, Rect2 slopeRect, Vec2 slopeOrientation, Vec2 velocity, Vec2* normal) {
  Vec2 sweptNormal = Vec2s::Zero;
  float sweptTime = sweptAABB(rect, slopeRect, velocity, &sweptNormal, 1.0f);
  
  if (sweptTime == 1.0f) {
    // rect misses the slope
    return 1.0f;
  }

  if (Mathf::signum(sweptNormal.x) == -Mathf::signum(slopeOrientation.x) ||
      Mathf::signum(sweptNormal.y) == -Mathf::signum(slopeOrientation.y))
  {
    // rect hits the slope on one of the flat sides
    if (normal != NULL) {
      *normal = sweptNormal;
    }
    return sweptTime;
  }

  // TODO: check for a slope collision
  if (normal != NULL) {
    *normal = sweptNormal;
  }
  return sweptTime;
}

Vec2 Physics::getPushVector(Vec2 point, Rect2 rect) {
  if (!rect.contains(point)) {
    return Vec2s::Zero;
  }

  float diffR = rect.right() - point.x;
  float diffL = point.x - rect.left();
  float diffD = rect.bottom() - point.y;
  float diffU = point.y - rect.top();

  if (diffR < diffL && diffR < diffD && diffR < diffU) {
    return Vec2(diffR, 0.0f);
  }
  else if (diffL < diffD && diffL < diffU) {
    return Vec2(-diffL, 0.0f);
  }
  else if (diffD < diffU) {
    return Vec2(0.0f, diffD);
  }
  else {
    return Vec2(0.0f, -diffU);
  }
}

/**
* Implementation for push vector calculation described here:
* https://hugobdesigner.blogspot.com/2014/10/making-simple-2d-physics-engine.html
* (collision side detection method 3)
*/
Vec2 Physics::getPushVector(Rect2 rectA, Rect2 rectB) {
  if (!rectA.intersects(rectB)) {
    return Vec2s::Zero;
  }

  float diffR = rectA.right() - rectB.left();
  float diffL = rectB.right() - rectA.left();
  float diffD = rectA.bottom() - rectB.top();
  float diffU = rectB.bottom() - rectA.top();

  if (diffR < diffL && diffR < diffD && diffR < diffU) {
    return Vec2(-diffR, 0.0f);
  }
  else if (diffL < diffD && diffL < diffU) {
    return Vec2(diffL, 0.0f);
  }
  else if (diffD < diffU) {
    return Vec2(0.0f, -diffD);
  }
  else {
    return Vec2(0.0f, diffU);
  }
}